using Gtk;
using TimerLogs.Models;

namespace TimerLogs.Widgets
{
	public class ProjectsTasks : Box
	{
		protected	ProjectsList	widgetProjectsList;
		protected	TasksList		widgetTasksList;
		protected	Company			company;
		protected	Box				binTasks;
		protected	Project			currentProject;
		protected	TimerLogs.Models.Task	currentTask;
		
		public		signal void	project_selected(Project project);
		public		signal void task_selected(TimerLogs.Models.Task task);
		
		public ProjectsTasks(Company c)
		{
			this.set_orientation(Orientation.HORIZONTAL);
			this.company = c;
			this.build();
			this.setEvents();
		}	
		protected void build()
		{
			this.widgetProjectsList = new ProjectsList(this.company);
			this.widgetProjectsList.title = "Listado de Proyectos";
			this.binTasks			= new Box(Orientation.VERTICAL, 0);
			this.pack_start(this.widgetProjectsList, true, true);
			this.pack_start(new Separator(Orientation.VERTICAL), false, true);
			this.pack_start(this.binTasks);
			this.show_all();
		}
		protected void setEvents()
		{
			this.widgetProjectsList.project_selected.connect( this.onProjectSelected );
		}
		protected void onProjectSelected(Project? p)
		{
			if( this.currentProject != null && this.currentProject.id == p.id )
				return;
				
			this.currentProject = p;
			//## clear box children
			this.binTasks.forall( (child) => 
			{
				this.binTasks.remove(child);
			});
			if( p == null )
				return;
				
			this.widgetTasksList = new TasksList(p);
			this.widgetTasksList.task_selected.connect( this.onTaskSelected );
			this.widgetTasksList.loadTasks();
			this.widgetTasksList.show_all();
			this.binTasks.pack_start(this.widgetTasksList, true, true);
			this.project_selected(p);
		}
		protected void onTaskSelected(TimerLogs.Models.Task? task)
		{
			if( this.currentTask != null && this.currentTask.id == task.id )
				return;
				
			this.currentTask = task;
			this.task_selected(task);
		}
	}
}
