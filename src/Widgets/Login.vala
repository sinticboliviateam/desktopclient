using Gtk;
using TimerLogs;
using TimerLogs.Api;
using TimerLogs.Classes;
using TimerLogs.Classes.SinticBolivia;
using TimerLogs.Models;

namespace TimerLogs.Widgets
{
	public class WidgetLogin : Box
	{
		protected	Entry		entryUsername;
		protected	Entry		entryPassword;
		protected	Button		buttonLogin;
		protected	Label		labelError;
		
		public		signal		void login_success(User user, string jwt);
		public		signal		void login_error();
		
		public WidgetLogin()
		{
			this.set_orientation(Orientation.VERTICAL);
			this.set_spacing(10);
			this.build();
			this.name	= "widget-login";
			this.get_style_context().add_class("white-widget");
			this.setEvents();
		}
		public void build()
		{
			var labelWelcome	= new Label("Bienvenido a Timerlogs");
			var labelSubtitle	= new Label("Inicio de Sesión");
			this.labelError		= new Label("");
			
			labelWelcome.get_style_context().add_class("black");
			labelSubtitle.get_style_context().add_class("black");
			this.labelError.get_style_context().add_class("alert");
			this.labelError.get_style_context().add_class("alert-danger");
			this.labelError.hide();
			
			labelWelcome.name	= "login-title";
			labelSubtitle.name	= "login-subtitle";
			
			this.entryUsername 	= new Entry();
			this.entryPassword	= new Entry();
			this.buttonLogin	= new Button.with_label("Login");
			this.buttonLogin.get_style_context().add_class("btn");
			this.buttonLogin.get_style_context().add_class("btn-primary");
			this.buttonLogin.set_data<string>("text", "Login");
			this.buttonLogin.set_data<string>("text_busy", "Validando credenciales...");
			
			var labelUsername	= new Label("Usuario");
			labelUsername.get_style_context().add_class("black");
			labelUsername.xalign = 0;
			var labelPassword	= new Label("Contraseña");
			labelPassword.get_style_context().add_class("black");
			labelPassword.xalign = 0;
			this.entryPassword.visibility = false;
			
			try
			{
				var imageLogo = new Image.from_pixbuf(
					new Gdk.Pixbuf.from_file_at_scale(tl_get_resource_path("images/logo-text.png"), 500, -1, true)
				);
				this.pack_start(imageLogo, false, false, 10);
			}
			catch(Error e)
			{
				tl_log("ERROR: %s".printf(e.message));
			}
			
			//this.get_style_context().add_class("");
			
			this.pack_start(labelWelcome, false, false);
			this.pack_start(labelSubtitle, false, false, 10);
			this.pack_start(labelUsername, !true, !true, 0);
			this.pack_start(this.entryUsername, !true, !true, 0);
			this.pack_start(labelPassword, !true, !true, 0);
			this.pack_start(this.entryPassword, !true, !true, 0);
			this.pack_start(this.labelError, false, true, 10);
			this.pack_start(this.buttonLogin, false, false);
		}
		public void setEvents()
		{
			this.entryUsername.key_press_event.connect( (evt) => 
			{
				if( /*evt.keyval == 65289 ||*/ evt.keyval == 65293 )
				{
					this.entryPassword.grab_focus_without_selecting();
				}
				return false;
			});
			this.entryPassword.key_press_event.connect( (evt) => 
			{
				print("KeyPres: %u\n", evt.keyval);
				if( /*evt.keyval == 65289 ||*/ evt.keyval == 65293 )
				{
					this.buttonLogin.clicked();
				}
				return false;
			});
			this.buttonLogin.clicked.connect(this.onButtonLoginClicked);
		}
		public void onButtonLoginClicked()
		{
			this.buttonLogin.label	= this.buttonLogin.get_data<string>("text_busy");
			this.buttonLogin.sensitive = false;
			this.labelError.label = "";
			this.labelError.hide();
			
			var api = new ApiUsers();
			ApiResponse? res = null;
			try
			{
				ThreadFunc<void> apiTask = () => 
				{
					res = api.login(this.entryUsername.text.strip(), this.entryPassword.text.strip());
					var obj = res.getObject();
					if( res == null || res.http_code != 200 )
					{
						GLib.Idle.add( () => 
						{
							this.buttonLogin.sensitive 	= true;
							this.buttonLogin.label		= this.buttonLogin.get_data<string>("text");
							this.labelError.label 		= "Ocurrio un error desconocido al procesar la solicitud.\nIntentelo nuevamente\nERROR: %u\n"
															.printf(res.http_code);
							if( res.hasError() )
								this.labelError.label = res.getError();
							this.labelError.show();
							
							return false;
						});
						
						return;
					}
					var odata	= obj.get_object_member("data");
					string jwt 	= odata.get_string_member("token");
					//##save jwt
					var kfile 	= (KeyFile)SBGlobals.GetValue("kfile");
					kfile.set_string("Authentication", "jwt", jwt);
					//print("Token: %s\n", jwt);
					save_config();
					var user = new User();
					user.loadJsonObject(odata.get_object_member("user"));
					SBGlobals.SetValue("user", user);
					//print("User: %s\n", user.username);
					GLib.Idle.add( () => 
					{
						this.buttonLogin.label	= this.buttonLogin.get_data<string>("text");
						this.buttonLogin.sensitive = true;
						this.login_success(user, jwt);
						return false;
					});
				};
				new Thread<void>.try("api_login", (owned)apiTask);
			}
			catch(Error e)
			{
			}
		}
		public void reset()
		{
			this.entryUsername.text		= "";
			this.entryPassword.text		= "";
			this.labelError.label 	= "";
			this.labelError.hide();
			this.buttonLogin.label	= this.buttonLogin.get_data<string>("text");
		}
	}
}
