using Gtk;

namespace TimerLogs.Widgets
{
	public class InactivityAlert : Box
	{
		public InactivityAlert()
		{
			this.orientation = Orientation.VERTICAL;
			this.build();
		}
		protected void build()
		{
			var label = new Label("Hola!! Parece que has estado muy callado. Aun estas trabajando?");
			var pixbuf = new Gdk.Pixbuf.from_file_at_scale(tl_get_resource_path("images/inactivity.jpg"), 400, 400, true);
			this.pack_start(label, false, true, 5);
			this.pack_start(new Image.from_pixbuf(pixbuf), false, true, 5);
		}
	}
}
