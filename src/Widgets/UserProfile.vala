using Gtk;
using GLib;
using TimerLogs.Models;

namespace TimerLogs.Widgets
{
	public class UserProfile : Box
	{
		protected	StackSwitcher	stackSwitcher;
		protected	Stack			stack;
		protected	Entry			entryFirstname;
		protected	Entry			entryLastname;
		protected	Entry			entryEmail;
		protected	Entry			entryPassword;
		protected	Entry			entryRPassword;
		
		public		User			user {get;set;}
		
		public UserProfile()
		{
			this.orientation = Orientation.VERTICAL;
			this.spacing = 5;
			this.get_style_context().add_class("box-user-profile");
			this.build();
		}
		protected void build()
		{
			this.stackSwitcher			= new StackSwitcher();
			this.stack					= new Stack(){
				transition_type = StackTransitionType.SLIDE_LEFT_RIGHT
			};
			this.stackSwitcher.stack 	= this.stack;
			this.stack.add_titled(this.buildGeneralInfo(), "general", "Información General");
			this.stack.add_titled(this.buildPassword(), "password", "Contraseña");
			this.pack_start(this.stackSwitcher, false, true, 10);
			this.pack_start(this.stack, true, true, 10);
		}
		protected Box buildGeneralInfo()
		{
			this.entryFirstname = new Entry();
			this.entryLastname	= new Entry();
			this.entryEmail		= new Entry();
			
			var box = new Box(Orientation.VERTICAL, 5);
			box.pack_start(new Label("Nombre"){xalign = 0.0f}, false, true);
			box.pack_start(this.entryFirstname, false, true);
			box.pack_start(new Label("Apellido"){xalign = 0.0f}, false, true);
			box.pack_start(this.entryLastname, false, true);
			box.pack_start(new Label("Email"){xalign = 0.0f}, false, true);
			box.pack_start(this.entryEmail, false, true);
			
			return box;
		}
		protected Box buildPassword()
		{
			this.entryPassword	= new Entry(){
				invisible_char = '*',
				invisible_char_set = true,
				visibility = false,
				placeholder_text = "Dejar en blanco para no actualizar"
			};
			this.entryRPassword	= new Entry(){
				invisible_char = '*',
				invisible_char_set = true,
				visibility = false,
				placeholder_text = "Dejar en blanco para no actualizar"
			};
			var box = new Box(Orientation.VERTICAL, 5);
			box.pack_start(new Label("Contraseña"){
				xalign = 0.0f
			}, false, true);
			box.pack_start(this.entryPassword, false, true);
			box.pack_start(new Label("Repetir Contraseña"){
				xalign = 0.0f
			}, false, true);
			box.pack_start(this.entryRPassword, false, true);
			
			return box;
		}
		public void setUser(User usr)
		{
			this.user = usr;
			this.entryFirstname.text = this.user.first_name;
			this.entryLastname.text = this.user.last_name;
			this.entryEmail.text	= this.user.email;
		}
		public void save()
		{
			
		}
	}
}
