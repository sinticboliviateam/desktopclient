using Gtk;
using TimerLogs.Models;
using Gee;

namespace TimerLogs.Widgets
{
	public class TasksList : Overlay
	{
		protected	Project		project;
		protected	ArrayList<TimerLogs.Models.Task>	tasks;
		protected	ListBox		listbox;
		protected	Box			mainBox;
		protected	Box?		boxLoading;
		
		public	signal void task_selected(TimerLogs.Models.Task? task);
		
		public TasksList(Project p)
		{
			this.mainBox = new Box(Orientation.VERTICAL, 0);
			this.mainBox.name = "box-project-tasks-" + project.id.to_string();
			this.mainBox.get_style_context().add_class("items-list-container");
			this.boxLoading = new Box(Orientation.VERTICAL, 5);
			
			this.project = p;
			this.tasks = new ArrayList<TimerLogs.Models.Task>();
			this.listbox = new ListBox();
			this.listbox.row_activated.connect(this.onTaskSelected);
			this.mainBox.pack_start(listbox, true, true);
			this.add(this.mainBox);
		}
		public void loadTasks()
		{
			tl_log(@"Loading tasks for: $(this.project.name)\n");
			/*
			//## clear box children
			this.forall( (child) => 
			{
				box.remove(child);
			});
			*/
			
			var spinner = new Spinner();
			spinner.start();
			this.boxLoading.pack_start(new Label("Obteniendo tareas..."){xalign = 0.5f}, false, false, 0);
			this.boxLoading.pack_start(spinner, !true, !true, 0);
			this.boxLoading.show_all();
			this.mainBox.pack_start(boxLoading, true, true, 0);
			this.mainBox.show_all();
			
			this.getTasks();
			/*
			box.pack_start(new Label("Lista de Tareas"){xalign = 0.0f}, !true, true);
			box.pack_start(listBox, true, true);
	
			box.pack_start(new Label("El proyecto no tiene tareas asignadas"), true, true, 0);
			*/
			
		}
		protected void getTasks()
		{
			this.listbox.hide();
			//SourceFunc callback = this.getTasks.callback;
			var api = TimerLogs.Api.ApiTimerLogs.getInstance();
			
			ThreadFunc<void> apiTask = () => 
			{
				var res = api.getProjectTasks((int)this.project.id);
				//tl_log(res.response);
				var data = res.getArrayData();
				int i = 0;
				foreach(var td in data.get_elements())
				{
					var t = td.get_object();
					var task = new TimerLogs.Models.Task();
					task.loadJsonObject(t);
					this.tasks.add(task);
					
					GLib.Idle.add( () => 
					{
						this.addTaskRow(task, i);
						i++;
						return false;
					} );
				}
				//Idle.add( (owned) callback );
				
			};
			new Thread<void>("api_tasks", (owned)apiTask);
		}
		protected void addTaskRow(TimerLogs.Models.Task task, int index)
		{
			if( this.boxLoading != null )
			{
				this.boxLoading.destroy();
				this.boxLoading = null;
			}
			var boxTask 			= new Box(Orientation.VERTICAL, 0);
			var boxText				= new Box(Orientation.HORIZONTAL, 5);
			var boxButtons			= new Box(Orientation.HORIZONTAL, 3);
			var labelTaskName 		= new Label(task.name)
			{
				xalign 	= 0
			};
			var btnViewDetails = new Button.with_label("Ver detalles");
			btnViewDetails.set_data<int>("task_index", index);
			btnViewDetails.get_style_context().add_class("btn");
			btnViewDetails.get_style_context().add_class("btn-primary");
			btnViewDetails.clicked.connect( () => 
			{
				this.onBtnViewDetailsClicked(btnViewDetails);
			} );
			
			boxText.pack_start(labelTaskName, true, true);
			boxButtons.pack_start(btnViewDetails, false, false);
			boxTask.pack_start(boxText, false, true);
			boxTask.pack_start(boxButtons, !true, true);
			
			var listboxRow = new ListBoxRow();
			listboxRow.add(boxTask);
			listboxRow.activatable = true;
			listboxRow.selectable = true;
			listboxRow.set_data<int>("task_index", index);
			//listboxRow.activate.connect(this.onTaskSelected);
			this.listbox.insert(listboxRow, -1);
			this.listbox.show_all();
		}
		protected void onTaskSelected(ListBoxRow row)
		{
			int task_index = row.get_data<int>("task_index");
			var task = this.tasks.get(task_index);
			//tl_log(@"Task selected: $(task.name)\n");
			this.task_selected(task);
		}
		protected void onBtnViewDetailsClicked(Button btn)
		{
			int index = btn.get_data<int>("task_index");
			var task = this.tasks.get(index);
			print("Task: %s, Description: %s\n", task.name, task.description);
			var overlay = new Overlay();
			TextBuffer buffer = new TextBuffer(null);
			buffer.text = task.description;
			if( buffer.text.strip().length <= 0 )
				buffer.text = "La tarea no contiene descripción";
				
			Button btnBack = new Button.with_label("Volver");
			btnBack.get_style_context().add_class("btn");
			btnBack.get_style_context().add_class("btn-danger");
			btnBack.clicked.connect( () => 
			{
				overlay.destroy();
			});
			var box = new Box(Orientation.VERTICAL, 5);
			box.get_style_context().add_class("bg-white");
			box.get_style_context().add_class("p-5");
			box.pack_start(btnBack, false, false, 0);
			box.pack_start(
				new Label(task.name){
					xalign = 0.0f
				}, 
				false, true, 0
			);
			box.pack_start(new TextView.with_buffer(buffer){
				editable = false,
				wrap_mode = WrapMode.WORD_CHAR
			}, true, true, 0);
			overlay.add(box);
			overlay.show_all();
			this.add_overlay(overlay);
			
		}
	}
}
