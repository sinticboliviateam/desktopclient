using GLib;
using Gtk;
using TimerLogs.Classes;

namespace TimerLogs.Widgets
{
	public class Timer : Box
	{
		protected	Label	labelHours;
		protected	Label	labelMinutes;
		protected	Label	labelSeconds;
		protected	uint	timeout;
		protected	uint	hours	= 0;
		protected	uint	minutes = 0;
		protected	uint	seconds = 0;
		
		protected	uint		secondsElapsed = 0;
		protected	uint		intervalSeconds = 0;
		protected	DateTime?	workTimeStart 	= null;
		protected	DateTime?	timeStart		= null;
		protected	DateTime?	timeEnd			= null;
		protected	TimeData?	workedTimeData	= null;
		public		bool running {get;set;default = false;}
		
		public Timer()
		{
			this.set_orientation(Orientation.HORIZONTAL);
			this.set_spacing(0);
			this.name = "timerlogs-timer";
			this.halign = Align.START;
			this.build();
		}
		protected void build()
		{
			this.labelHours 			= new Label("00");
			this.labelHours.xalign 		= 0;
			this.labelMinutes 			= new Label("00");
			this.labelMinutes.xalign 	= 0;
			this.labelSeconds			= new Label("00");
			this.labelSeconds.xalign 	= 0;
			
			this.pack_start(this.labelHours, false, false, 0);
			this.pack_start(new Label(":"), false, false, 0);
			this.pack_start(this.labelMinutes, false, false, 0);
			this.pack_start(new Label(":"));
			this.pack_start(this.labelSeconds, false, false, 0);
			this.show_all();
		}
		public void setWorkedTimeData(TimeData wtd)
		{
			var ctime = new DateTime.now_local();
			this.workedTimeData = wtd;
			this.workTimeStart	= new DateTime.from_unix_local(ctime.to_unix() - wtd.totalSeconds);
			this.setTime(wtd);
		}
		public void start()
		{
			if( this.running )
				return;
				
			this.running = true;
			if( this.workTimeStart == null )
				this.workTimeStart = new DateTime.now_local();
				
			this.timeStart = new DateTime.now_local();
			ThreadFunc<void> timerThread = () => 
			{
				this.timeout = GLib.Timeout.add(1000, () => 
				{
					this.seconds++;
					//print("Timer: %u\n".printf(this.seconds));
					if( this.seconds >= 60 )
					{
						this.seconds = 0;
						this.minutes++;
						if( this.minutes >= 60 )
						{
							this.minutes = 0;
							this.hours++;
						}
					}
					GLib.Idle.add( () => 
					{
						this.update();
						return false;
					});
					
					return true;
				}, GLib.Priority.HIGH);
			};
			try
			{
				new Thread<void>.try("thread_timer", (owned)timerThread);
			}
			catch(GLib.Error e)
			{
				tl_log("ERROR: $(e.message)");
			}
			
		}
		public void stop()
		{
			this.running = false;
			this.seconds 	= 0;
			this.minutes 	= 0;
			this.hours 		= 0;
			this.update();
		}
		/**
		 * Puase the current timer interval
		 */
		public void pause()
		{
			this.running = false;
			
			GLib.Source.remove(this.timeout);
			this.timeEnd 		= new DateTime.now_local();
			int64 diff			= this.timeEnd.to_unix() - this.workTimeStart.to_unix();
			this.secondsElapsed = (uint)diff;
			var td 				= new TimeData.fromSeconds(this.secondsElapsed);
			this.setTime(td);
			
			/*
			tl_log("TIMER STOP\nSTART TIME: %s\nEND TIME: %s\nSECONDS ELAPSED: %s\nGLOBAL TIME WORKED: %u\n".printf(
				this.timeStart.format("%Y-%m-%d %H:%M:%S"), 
				this.timeEnd.format("%Y-%m-%d %H:%M:%S"),
				diff.to_string(),
				this.timeEnd.to_unix() - this.workTimeStart.to_unix()
			));
			var cdate = new DateTime.now_local ();
			print("[%s]: Timer update: %s:%s:%s\n", cdate.format("%Y-%m-%d %H:%M:%S"), this.labelHours.label, this.labelMinutes.label, this.labelSeconds.label);
			*/
		}
		public void update()
		{
			this.labelSeconds.label = this.seconds < 10 ? "0%u".printf(this.seconds) : "%u".printf(this.seconds);
			this.labelMinutes.label = this.minutes < 10 ? "0%u".printf(this.minutes) : "%u".printf(this.minutes);
			this.labelHours.label 	= this.hours < 10 ? "0%u".printf(this.hours) : "%u".printf(this.hours);
			/*
			while (Gtk.events_pending ())
				Gtk.main_iteration ();
			*/
			//var cdate = new DateTime.now_local ();
			
			//print("[%s]: Timer update: %s:%s:%s\n", cdate.format("%Y-%m-%d %H:%M:%S"), this.labelHours.label, this.labelMinutes.label, this.labelSeconds.label);
		}
		public void updateWorkedTime()
		{
			if( !this.running )
				return;
				
			this.setTime(this.getWorkedTime());
		}
		public void resetAll()
		{
			if( this.timeout > 0 )
				GLib.Source.remove(this.timeout);
			this.timeout			= 0;
			this.workTimeStart 		= null;
			this.seconds 			= 0;
			this.minutes 			= 0;
			this.hours 				= 0;
			this.secondsElapsed		= 0;
			this.intervalSeconds	= 0;
			this.update();
		}
		public void resetInterval()
		{
			this.timeStart 	= new DateTime.now_local();
			this.timeEnd	= null;
		}
		public void setTime(TimeData timedata)
		{
			this.hours 		= timedata.hour;
			this.minutes	= timedata.minute;
			this.seconds	= timedata.second;
			this.update();
		}
		public TimeData getWorkedTime()
		{
			if( this.workTimeStart == null )
			{
				return new TimeData();
			}
			var _timeEnd 	= new DateTime.now_local();
			int64 diff 		= _timeEnd.to_unix() - this.workTimeStart.to_unix();
			var td 			= new TimeData.fromSeconds((uint)diff);
			
			return td;
		}
		public TimeData getIntervalTime()
		{
			if( this.timeEnd == null )
				this.timeEnd = new DateTime.now_local();
				
			int64 diff 		= this.timeEnd.to_unix() - this.timeStart.to_unix();
			var td 			= new TimeData.fromSeconds((uint)diff);
			
			return td;
		}
	}
}
