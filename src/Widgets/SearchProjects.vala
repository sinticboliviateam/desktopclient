using Gtk;
using GLib;

namespace TimerLogs.Widgets
{
	public class SearchProjects : Box
	{
		protected	string	keyword;
		protected	Entry	entrySearch;
		
		public SearchProjects()
		{
			this.set_orientation(Orientation.VERTICAL);
			this.build();
		}
		protected void build()
		{
			this.entrySearch = new Entry(){
				placeholder_text = "Buscar proyecto..."
			};
			//this.entrySearch.show();
			this.pack_start(this.entrySearch, false, true, 5);
			
		}
	}
}
