using Gtk;
using Gee;
using TimerLogs.Api;
using TimerLogs.Classes.SinticBolivia;
using TimerLogs.Models;

namespace TimerLogs.Widgets
{
	public class ProjectsList : Box
	{
		protected	StackSidebar		sidebar;
		protected	Stack				stack;
		protected	ApiTimerLogs		api;//	= new ApiTimerLogs();
		protected	ArrayList<Project> 	projects;
		protected	unowned Company		company;
		protected	Button				buttonRefreshProjects;
		protected	Box					boxProjectTitle;
		//protected	Box					boxProjects;
		protected	TasksList?			widgetTasksList;
		protected	Label				labelTitle;
		protected	ListBox				listboxProjects;
		
		public		string title 
		{
			get {
				return this.labelTitle.label;
			}
			set {
				this.labelTitle.label = value;
			}
			//default = "";
		}
		
		//##events
		public	signal void project_selected(Project? project);
		
		
		public ProjectsList(Company c)
		{
			this.api = ApiTimerLogs.getInstance();
			
			this.company = c;
			this.set_orientation(Orientation.VERTICAL);
			this.spacing = 5;
			//this.homogeneous = true;
			//this.get_style_context().add_class('');
			this.name = "projects-list";
			this.get_style_context().add_class("items-list-container");
			this.build();
			this.onButtonRefreshProjectsClicked();
		}
		public void build()
		{
			this.boxProjectTitle	= new Box(Orientation.HORIZONTAL, 5);
			this.labelTitle			= new Label("")
			{
				xalign = 0.0f
			};
			this.labelTitle.get_style_context().add_class("label-title");
			//this.boxProjects			= new Box(Orientation.VERTICAL);
			this.buttonRefreshProjects	= new Button();
			this.buttonRefreshProjects.get_style_context().add_class("btn btn-success");
			this.buttonRefreshProjects.tooltip_text = "Refrescar proyectos";
			this.buttonRefreshProjects.image = new Image.from_file(tl_get_resource_path("images/refresh-24.png"));
			this.buttonRefreshProjects.clicked.connect( this.onButtonRefreshProjectsClicked );
			this.boxProjectTitle.pack_start(this.labelTitle, true, true);
			this.boxProjectTitle.pack_start(this.buttonRefreshProjects, false, true);
			
			
			var searchProjects = new SearchProjects();
			searchProjects.show_all();
			
			this.listboxProjects = new ListBox();
			this.pack_start(this.boxProjectTitle, false, true);
			this.pack_start(searchProjects, false, true);
			this.pack_start(this.listboxProjects, true, true);
			/*
			bool flag = false;
			this.sidebar.set_focus_child.connect( (child) => 
			{
				if( !flag )
				{
					print("Child name: %s\n", child.name);
					var viewport = (child as ScrolledWindow).get_child() as Viewport;
					print("- Child name: %s\n", viewport.get_child().name);
					
					flag = true;
				}				
			});
			* */
			this.listboxProjects.row_activated.connect( this.onProjectSelected );
			//tl_log("ProjectsList.build end");
		}
		protected void onButtonRefreshProjectsClicked()
		{
			this.getProjects();
			this.project_selected(null);
		}
		protected void onProjectSelected(ListBoxRow row)
		{
			int index = row.get_data<int>("project_index");
			Project pSelected = this.projects.get(index);
			
			this.project_selected(pSelected);
		}
		protected void getProjects()
		{
			this.projects 	= new ArrayList<Project>();
			//## clear box children
			this.listboxProjects.forall( (child) => 
			{
				this.listboxProjects.remove(child);
			});
			ThreadFunc<void> threadProjects = () => 
			{
				var res 	= this.api.getProjects(this.company.id);
				var data	= res.getArrayData();
				int i = 0;
				foreach(var pn in data.get_elements())
				{
					var p 		= pn.get_object();
					var project = new Project();
					project.loadJsonObject(p);
					this.projects.add(project);
					GLib.Idle.add( () => 
					{
						this.addProjectRow(project, i);
						i++;
						return false;
					});
				}
			};
			new Thread<void>("thread_projects", (owned)threadProjects);
			
		}
		protected void addProjectRow(Project project, int index)
		{
			var boxProject 			= new Box(Orientation.HORIZONTAL, 5);
			var labelTaskName 		= new Label(project.name)
			{
				xalign 	= 0
			};
			boxProject.pack_start(labelTaskName, false, true);
			var listboxRow = new ListBoxRow();
			listboxRow.add(boxProject);
			listboxRow.activatable = true;
			listboxRow.selectable = true;
			listboxRow.set_data<int>("project_index", index);
			//listboxRow.activate.connect(this.onTaskSelected);
			this.listboxProjects.insert(listboxRow, -1);
			this.listboxProjects.show_all();
		}
	}
}
