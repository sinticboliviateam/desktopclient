using Gtk;
using Gee;

namespace TimerLogs.Widgets
{
	//protected	ArrayList<string[]>	menuItems;
	
	public class MenuStatusIcon : Gtk.Menu
	{
		protected	Gtk.MenuItem	menuItemOpen;
		protected	Gtk.MenuItem	menuItemAbout;
		protected	Gtk.MenuItem	menuItemExit;
		
		public MenuStatusIcon()
		{
			/*
			this.menuItems = new ArrayLis<string[]>();
			this.menuItems.add(new string[]{"About TimerLogs", ""};);
			this.menuItems.add(new string[]{"About TimerLogs", ""};);
			*/
			this.menuItemOpen	= new Gtk.MenuItem.with_label("Abrir TimerLogs");
			this.menuItemAbout 	= new Gtk.MenuItem.with_label("Acerca de TimerLogs");
			this.menuItemExit 	= new Gtk.MenuItem.with_label("Salir");
			this.append(this.menuItemOpen);
			this.append(this.menuItemAbout);
			this.append(this.menuItemExit);
			this.menuItemOpen.activate.connect(this.OnMenuItemOpenClicked);
		}
		protected void OnMenuItemOpenClicked()
		{
		}
	}
}
