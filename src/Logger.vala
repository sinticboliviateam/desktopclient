using Gtk;
using Gdk;
using TimerLogs.Classes;
using TimerLogs.Models;
using TimerLogs.Api;
using TimerLogs.Classes.SinticBolivia;
using TimerLogs.Widgets;

#if __WIN32__

#endif
#if __MACOS__
//using TimerLogs.Macos.Classes;
#endif

namespace TimerLogs
{
	public class Logger
	{
		protected	Gtk.Window		window;
		protected	uint			timeout;
		#if __WIN32__
		protected	LoggerWin		loggerWin;
		#endif
		
		#if __MACOS__
		protected	LoggerMacos		loggerMacos;
		public		bool			permissionsOk = false;
		#endif
		
		protected	User			user;
		public		bool			logSent = false;
		public		uint			interval = 180; //capture interval in seconds
		
		protected	Project 					_project;
		protected	TimerLogs.Models.Task 		_task;
		protected	TimerLogs.Widgets.Timer 	_timer;
		
		public Logger(MainWindow win, User? usr)
		{
			this.window = win;
			if( usr != null )
				this.user	= usr;
			
			#if __WIN32__
			this.loggerWin	= new LoggerWin();
			#endif
			
			#if __MACOS__
			this.loggerMacos	= new LoggerMacos();
			this.permissionsOk	= this.loggerMacos.askPermissions();
			#endif
			
			win.on_window_focus.connect(this.OnWindowFocus);
		}
		protected void OnWindowFocus()
		{
			stdout.printf("LOGGER -> WINDOW FOCUS, WE NEED TO REFRESH SOME UI\n");
			
		}
		public void SetUser(User usr)
		{
			this.user = usr;
		}
		public void Start(Project project, TimerLogs.Models.Task task, TimerLogs.Widgets.Timer timer)
		{
			this._project 	= project;
			this._task 		= task;
			this._timer		= timer;
			
			this.JustStart();
		}
		public void JustStart()
		{
			#if __WIN32__
			this.loggerWin.startKMCapture();
			#endif
			
			#if __MACOS__
			this.loggerMacos.startKMCapture();
			#endif
			this.timeout = GLib.Timeout.add(this.interval * 1000, () => 
			{
				this.SendData("continue_task");
				return true;
			}, GLib.Priority.HIGH);
			this.SendData("init_task");
		}
		public void Stop()
		{
			if( this.timeout > 0 )
				GLib.Source.remove(this.timeout);
			this.SendData("stop_task");
			#if __WIN32__
			this.loggerWin.stopKMCapture();
			#endif
			#if __MACOS__
			this.loggerMacos.stopKMCapture();
			#endif
		}
		public uint8[] CaptureScreen()
		{
			uint8[] buffer = {};
			#if __WIN32__
			this.loggerWin.captureDesktopToBuffer(out buffer);
			#endif
			
			#if __MACOS__
			this.loggerMacos.captureDesktopToBuffer(out buffer);
			#endif
			
			return buffer;
		}
		public AppData GetCurrentApp()
		{
			AppData appData = new AppData();
			
			#if __MACOS__
			appData = this.loggerMacos.getCurrentAppData();
			#endif
			
			#if __WIN32__
			appData = this.loggerWin.getCurrentAppData();
			#endif
			
			return appData;
		}
		protected KeyboardMouseData CaptureKMData()
		{
			KeyboardMouseData kmData = null;
			
			#if __WIN32__
			kmData = this.loggerWin.captureKMData();
			#endif
			
			#if __MACOS__
			kmData = this.loggerMacos.captureKMData();
			#endif
			return kmData;
		}
		protected UserData CollectUserData()
		{
			var udata 			= new UserData();
			udata.user_id		= this.user.user_id;
			udata.username		= this.user.username;
			udata.project_id 	= this._project.id;
			udata.task_id		= this._task.id;
			udata.ttype			= "init_task";
			udata.screenshot	= Base64.encode(this.CaptureScreen());
			udata.kmData		= this.CaptureKMData();
			udata.appData		= this.GetCurrentApp();
			var timeData 		= this._timer.getIntervalTime();
			this._timer.resetInterval();
			var datetime		= new DateTime.now_local();
			udata.seconds		= timeData.totalSeconds;
			udata.taskData 		= new TaskData();
			udata.taskData.task_date 			= datetime.format("%Y-%m-%d");
			udata.taskData.task_accumulated_time = timeData.getTotalHours();
			udata.taskData.user_id 				= this.user.user_id;
			udata.taskData.task_id 				= this._task.id;
			
			return udata;
		}
		public void SendData(string type = "init_task")
		{
			var userData = this.CollectUserData();
			userData.ttype = type;
			this.TrackData(userData);
			if( userData.kmData.inactivity >= 180 )
			{
				this.Stop();
				this._timer.pause();
				this.ShowInactivityAlert();
			}
		}
		/**
		 * Send the user captured data to timerlogs server
		 *
		 */
		protected void TrackData(UserData udata)
		{
			try
			{
				ThreadFunc<void> apiTask = () => 
				{
					tl_log("TRACK DATA THREAD START\n");
					tl_log("LOGGER USER DATA:\n %s\n".printf(udata.kmData.toJson()));
					
					//var kfile 	= (KeyFile)SBGlobals.GetValue("kfile");
					//string jwt 			= kfile.get_string("Authentication", "jwt");
					ApiTimerLogs api	= ApiTimerLogs.getInstance(); //new ApiTimerLogs();
					//api.token			= jwt;
					var res = api.persistTrack(udata.toJson());
					tl_log("TRACK DATA THREAD FINISHED\nRESPONSE:\n%s\n".printf(res.response));
					
				};
				new Thread<void>.try("api_tasks", (owned)apiTask);
			}
			catch(GLib.Error e)
			{
				tl_log("TRACK DATA ERROR: %s\n".printf(e.message));
			}
			
		}
		protected void ShowInactivityAlert()
		{
			var widgetInactivity = new InactivityAlert();
			widgetInactivity.show_all();
			var dlg = new Dialog(){
				modal = true,
				decorated = false,
				window_position = WindowPosition.CENTER_ALWAYS
			};
			dlg.set_keep_above(true);
			dlg.stick();
			var btnReject = dlg.add_button("No estoy trabajando", ResponseType.REJECT);
			var btnWorking = dlg.add_button("Si, estoy trabajando", ResponseType.YES);
			btnReject.get_style_context().add_class("btn");
			btnReject.get_style_context().add_class("btn-danger");
			btnWorking.get_style_context().add_class("btn");
			btnWorking.get_style_context().add_class("btn-success");
			
			dlg.get_content_area().pack_start(widgetInactivity);
			int res = dlg.run();
			dlg.destroy();
			if( res == ResponseType.REJECT )
			{
			}
			else if( res == ResponseType.YES )
			{
				this.JustStart();
				this._timer.start();
			}
		}
	}
}
