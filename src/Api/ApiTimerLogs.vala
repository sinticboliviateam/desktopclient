using Gee;
using Soup;
using TimerLogs.Classes;
using TimerLogs.Classes.SinticBolivia;

namespace TimerLogs.Api
{
	public class ApiTimerLogs
	{
		protected	string apiBase;
		public		string	token = "";
		public		uint		http_code = 0;
		
		protected	static	ApiTimerLogs	instance;
		
		public static ApiTimerLogs getInstance()
		{
			if( instance == null )
			{
				instance = new ApiTimerLogs();
			}
			
			return instance;
		}
		
		protected ApiTimerLogs()
		{
			var kfile 		= (KeyFile)SBGlobals.GetValue("kfile");
			try
			{
				this.apiBase 	= kfile.get_string("Settings", "api_url"); //"http://192.168.1.13/SBFramework/api";
				this.token		= kfile.get_string("Authentication", "jwt");
			}
			catch(GLib.KeyFileError e)
			{
				print(e.message);
			}
			
		}
		public string request(string url, string method = "GET",  string? data = null)
		{
			var session = new Soup.Session();
			session.ssl_strict = false;
			var message = new Soup.Message(method, url);
			message.request_headers.append("Content-Type", "application/json");
			if( this.token.strip().length > 0	)
				message.request_headers.append("Authorization", "Bearer " + this.token);
			if( (method == "POST" || method == "PUT") && data != null )
			{
				//message.request_body.append(MemoryUse.COPY, data.data);
				message.request_body.append_take(data.data);
			}
			print("Sending request: %s\n", url);
			this.http_code = session.send_message(message);
			print("REQUEST RESPONSE CODE: %u\n", this.http_code);
			if( this.http_code < 100 )
			{
				tl_log("PHRASE: %s\n".printf(message.reason_phrase));
			}
			string response = (string)message.response_body.flatten().data;
			
			return response;
		}
		public ApiResponse get(string endpoint)
		{
			string url = this.apiBase + endpoint;
			string rdata = this.request(url);
			return new ApiResponse(rdata, this.http_code);
		}
		public ApiResponse post(string endpoint, string data)
		{
			string url = this.apiBase + endpoint;
			
			string rdata = this.request(url, "POST", data);
			var  res = new ApiResponse(rdata, this.http_code);
			
			return res;
		}
		public ApiResponse put(string endpoint, string data)
		{
			string url = this.apiBase + endpoint;
			
			string rdata = this.request(url, "PUT", data);
			
			return new ApiResponse(rdata, this.http_code);
		}
		public ApiResponse delete(string endpoint)
		{
			string url = this.apiBase + endpoint;
			
			string rdata = this.request(url, "DELETE");
			
			return new ApiResponse(rdata, this.http_code);
		}
		public ApiResponse getProjects(int64 companyId)
		{
			string url 		= this.apiBase + @"/timerlogger/myprojects/$companyId";
			string rdata 	= this.request(url, "GET");
			
			return new ApiResponse(rdata, this.http_code);
		}
		public ApiResponse getProjectTasks(int pid)
		{
			string url		= this.apiBase + @"/timerlogger/projects/$pid/tasks";
			string rdata	= this.request(url, "GET");
			//print(rdata);
			return new ApiResponse(rdata, this.http_code);
		}
		public ApiResponse persistTrack(string json)
		{
			string url	= "/timerlogger/tracking";
			return this.post(url, json);
		}
		public ApiResponse saveTaskData(string json)
		{
			string url = "/timerlogger/tracking/savedata";
			print(json);
			return this.post(url, json);
		}
		public ApiResponse getSettings(int companyId)
		{
			string url = "/timerlogger/companies/%d/settings".printf(companyId);
			return this.get(url);
		}
		public ApiResponse getCompanies()
		{
			string url = "/timerlogger/tracking/companies";
			
			return this.get(url);
		}
	}
}
