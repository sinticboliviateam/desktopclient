using TimerLogs.Classes;

namespace TimerLogs.Api
{
	public class ApiUsers : ApiTimerLogs
	{
		public ApiUsers()
		{
			base();
		}
		public ApiResponse login(string username, string password)
		{
			string endpoint = "/v1.0.0/users/get-token";
			string data = @"{\"username\": \"$username\", \"password\": \"$password\"}";
			print(data);
			ApiResponse res = this.post(endpoint, data);
			print(@"Response Code: $(res.http_code)\nResponse: $(res.response)\n");
			
			return res;
		}
		public ApiResponse profile(string jwt)
		{
			this.token = jwt;
			string endpoint = "/users/profile";
			var res = this.get(endpoint);
			print(@"Response: $(res.response)\n");
			return res;
		}
	}
}
