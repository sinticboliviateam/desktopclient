#include <X11/Xlib.h>
#include <X11/extensions/XInput.h>
#ifdef HAVE_XI2
#include <X11/extensions/XInput2.h>
#endif
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    Display *display;
    Window root_window;
    XEvent event;

    display = XOpenDisplay(NULL);
    if (display == NULL)
	{
		printf("Unable to connect to X server\n");
		exit(1);
	}
    //root_window = XRootWindow(display, 0);
	//unsigned long screen = DefaultScreen(display);
	//root_window = RootWindow(display, screen);
	/*
	XDevice *device = XOpenDevice(display, info->id);

	if (!device) {
		printf("unable to open device '%s'\n", dev_name);
		return 0;
	}
	*/
	
	display = XOpenDisplay(NULL);
	root_window = XDefaultRootWindow(display);
	
	int pointerId = XGrabPointer(display, root_window, False, ButtonReleaseMask, GrabModeAsync, 
         GrabModeAsync, None, None, CurrentTime);
    printf("Device id: %d\n", pointerId);
    /*
    int kpointerId = XGrabKeyboard(display, root_window, False, KeyPressMask, GrabModeAsync, CurrentTime);
    printf("Device id: %d\n", kpointerId);
    */
    /*
	XDevice *device = XOpenDevice(display, kpointerId);
	if (!device) 
	{
		printf("Unable to open device '%d'\n", kpointerId);
		return 0;
	}
	*/
	int state;
	XWindowAttributes attributes;

	//XGetInputFocus(display, &root_window, &state);
	printf("window id = %d\n", state);
	unsigned long eventMask = KeyPressMask | KeyReleaseMask | ButtonPressMask;
	XSelectInput(display, root_window, eventMask);

	printf("Starting event loop\n");
	setvbuf(stdout, NULL, _IOLBF, 0);
    while(1) 
    {
        XNextEvent( display, &event );
        printf("Event next\n");
        switch( event.type ) {
            case MotionNotify:
                printf("x %d y %d\n", event.xmotion.x, event.xmotion.y );
                break;
			case KeyPress:
				printf("Event KeyPres\n");
			break;
			default:
				printf("Invalid event: %u\n", event.type);
			break;
        }
    }
	XSync(display, False);
	XCloseDisplay(display);
    return 0;
}
