namespace TimerLogs.Models
{
	public class TaskData : Model, Json.Serializable
	{
		public	int64	user_id{get;set;}
		public	int64	task_id{get;set;}
		public	double 	task_accumulated_time {get;set;default = 0.0d;}
		public	string	task_date {get;set;}
		
		public TaskData()
		{
			this.task_accumulated_time = 0.0d;
		}
		public virtual Json.Node serialize_property(string property_name, Value value, ParamSpec pspec)
		{
			if( property_name == "task_accumulated_time" )
			{
				var node = new Json.Node(Json.NodeType.VALUE);
				node.set_string("%.4lf".printf(this.task_accumulated_time));
				
				return node;
			}
			return this.default_serialize_property(property_name, value, pspec);
		}
		public void loadJsonObject(Json.Object obj)
		{
			this.user_id				= obj.has_member("user_id") ? obj.get_int_member("user_id") : 0;
			this.task_id				= obj.has_member("task_id") ? obj.get_int_member("task_id") : 0;
			this.task_accumulated_time	= obj.has_member("task_accumulated_time") ? obj.get_double_member("task_accumulated_time") : 0d;
			this.task_date				= obj.has_member("task_date") ? obj.get_string_member("task_date") : "";
		}
	}
}
