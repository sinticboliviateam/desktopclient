using Gee;

namespace TimerLogs.Models
{
	public class User : Model, IModel
	{
		public	int64 	user_id{get;set;}
		public	string	first_name {get;set;}
		public	string	last_name {get;set;}
		public	string	username {get;set;}
		protected	string	pwd {get;set;}
		public	string	email {get;set;}
		public	string	status {get;set;}
		public	uint64	role_id {get;set;}
		public	int		store_id {get;set;}
		public	string	last_modification_date {get;set;}
		public	string	creation_date {get;set;}
		public	string	avatar {get;set;}

		public	HashMap<string , Value?>	meta{get;set;}
		
		public User()
		{
			
		}
		public void loadJsonObject(Json.Object obj)
		{
			this.user_id	= obj.get_int_member("user_id");
			this.first_name	= obj.get_string_member("first_name");
			this.last_name	= obj.get_string_member("last_name");
			this.username	= obj.get_string_member("username");
			this.pwd		= obj.get_string_member("pwd");
			this.email		= obj.get_string_member("email");
			this.status		= obj.get_string_member("status");
			this.role_id	= obj.get_int_member("role_id");
			this.avatar		= obj.get_string_member("avatar");
			this.store_id	= (int)obj.get_int_member("store_id");
		}
	}
}
