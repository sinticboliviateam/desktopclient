namespace TimerLogs.Models
{
	public class UserSettings : Object
	{
		public	long	capture_time{get;set;}
		public	long	inactivity_interval{get;set;}
		public	int		can_edit_time{get;set;}
		
		public UserSettings()
		{
		}
		public void loadJsonObject(Json.Object obj)
		{
			this.capture_time 			= (long)obj.get_int_member("capture_time");
			this.inactivity_interval	= (long)obj.get_int_member("inactivity_interval");
			this.can_edit_time			= (int)obj.get_int_member("can_edit_time");
		}
	}
}
