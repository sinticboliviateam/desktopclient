namespace TimerLogs.Models
{
	public class Task : Model
	{
		public		int64		id{get;set;}
		public		int64		project_id {get;set;}
		public		string		name {get;set;}
		public		string		description {get;set;}
		public		int			max_hours {get;set;}
		public		string		status {get;set;}
		public		TaskData	tdata {get;set;}
		
		public Task()
		{
			
		}
		public void loadJsonObject(Json.Object obj)
		{
			this.id				= obj.get_int_member("id");
			this.project_id		= obj.get_int_member("project_id");
			this.name			= obj.get_string_member("name");
			this.description	= obj.get_string_member("description");
			this.status			= obj.get_string_member("status");
			if( obj.has_member("tdata") )
			{
				this.tdata = new TaskData();
				this.tdata.loadJsonObject(obj.get_object_member("tdata"));
			}
		}
	}
}
