using Gee;

namespace TimerLogs.Models
{
	public class Project : Model, IModel
	{
		public	int64	id {get;set;}
		public	int64	company_id {get;set;}
		public	string	name {get;set;}
		public	string	description {get;set;}
		public	string	data {get;set;}
		public	string	status {get;set;}
		public	int		deleted {get;set;}
		
		public	ArrayList<Task> tasks;
		
		public Project()
		{
			this.tasks = new ArrayList<Task>();
		}
		public void loadJsonObject(Json.Object obj)
		{
			this.id				= obj.get_int_member("id");
			this.company_id		= obj.get_int_member("company_id");
			this.name			= obj.get_string_member("name");
			this.description	= obj.get_string_member("description");
			this.data			= obj.get_string_member("data");
			this.status			= obj.get_string_member("status");
			if( obj.has_member("tasks") )
			{
				foreach( var nodeT in obj.get_array_member("tasks").get_elements() )
				{
					var t = nodeT.get_object();
					var task = new Task();
					task.loadJsonObject(t);
					this.tasks.add(task);
				}
			}
		}
	}
}
