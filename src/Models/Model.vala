namespace TimerLogs.Models
{
	public abstract class Model : Object
	{
		public virtual void loadFromJson(string json)
		{
			try
			{
				var parser = new Json.Parser();
				parser.load_from_data(json, -1);
				var root_obj = parser.get_root().get_object();
				if( root_obj.has_member("data") )
				{
					var data_obj = root_obj.get_object_member("data");
					this.loadJsonObject(data_obj);
					return;
				}
				this.loadJsonObject(root_obj);
			}
			catch(GLib.Error e)
			{
				print(@"Model Error: $(e.message)");
			}
			
		}
		public virtual string toJson()
		{
			size_t length;
			
			string json = Json.gobject_to_data(this, out length);
			
			return json;
		}
		public virtual void loadJsonObject(Json.Object obj)
		{
			
		}
	}
}
