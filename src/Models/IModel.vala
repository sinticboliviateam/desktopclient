namespace TimerLogs.Models
{
	public interface IModel : Object
	{
		public abstract void loadFromJson(string json);
		public abstract void loadJsonObject(Json.Object obj);
		public abstract string toJson();
	}
}
