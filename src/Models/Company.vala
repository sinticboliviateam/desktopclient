namespace TimerLogs.Models
{
	public class Company : Model, IModel
	{
		public	int64	id{get;set;}
		public	string	name{get;set;}
		public	string	country{get;set;}
		public	string	address{get;set;}
		public	int64	membership_id{get;set;}
		public	string	time_zone{get;set;}
		public	string	status{get;set;}
		
		public	UserSettings	settings{get;set;}
		
		public Company()
		{
			
		}
		/*
		public void loadFromJson(string json)
		{
			var parser = Json.Parse();
			parser.load_from_data(json, -1);
			var root_obj = parser.get_root().get_object();
			if( root_obj.has_member("data") )
			{
				var data_obj = root_obj.get_object_member("data");
				this.loadJsonObject(data_obj);
				return;
			}
			this.loadJsonObject(root_obj);
		}
		*/
		public override void loadJsonObject(Json.Object obj)
		{
			this.id				= obj.get_int_member("id");
			this.name			= obj.get_string_member("name");
			this.country		= obj.get_string_member("country");
			this.address		= obj.get_string_member("address");
			this.membership_id	= obj.get_int_member("membership_id");
			this.time_zone		= obj.get_string_member("time_zone");
			this.status			= obj.get_string_member("status");
			if( obj.has_member("settings") )
			{
				this.settings = new UserSettings();
				this.settings.loadJsonObject(obj.get_object_member("settings"));
			}
		}
	}
}
