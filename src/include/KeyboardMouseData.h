#ifndef __HOOKS_DATA__
#define __HOOKS_DATA__

typedef struct hookApplicationData
{
	char            name[512];
	char            filename[512];
	char            process[1024];
	long			timestamp;
}HookApplicationData;

typedef struct hookKeyboardMouseData
{
	unsigned long			clicks;
	unsigned long			leftClicks;
	unsigned long			rightClicks;
	unsigned long			keydowns;
	double					keyboardInactivity;
	double					mouseInactivity;
	double					inactivity;
	HookApplicationData		**applications;
}HookKeyboardMouseData;

HookKeyboardMouseData* HookKeyboardMouseData_new(void);
void HookKeyboardMouseData_free(HookKeyboardMouseData*);


HookApplicationData* HookApplicationData_new(void);
void HookApplicationData_free(HookApplicationData*);

#endif
