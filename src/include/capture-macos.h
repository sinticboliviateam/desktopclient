#include "KeyboardMouseData.h"

#ifndef __MAC_CAPTURE__
#define __MAC_CAPTURE__
//##capture.h
void capture_desktop(char* filename);
void fast_capture_desktop(char* destination);
int check_accesibility_access(void);
void open_accesibility();
int check_capture_screen_access();
void tl_set_log_file(char* filename);

//##km.h
void start_km_capture(void);
void stop_km_capture(void);
HookKeyboardMouseData* getData(void);
HookApplicationData* getActiveWindowData(void);

#endif
