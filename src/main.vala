using Gtk;
using TimerLogs;
using TimerLogs.Classes.SinticBolivia;
using TimerLogs.Api;
using TimerLogs.Models;

string BASE_DIR;
string BIN_DIR;
string DS;
string API_BASE;
string DATA_DIR;
string LOG_FILENAME;
string CFG_DIR;
string RUNTIME_DIR;

#if __WIN32__
public extern void setMessageWindowHandler(void* gdkWindow);
int msg_h_added = 0;
#endif

void tl_check_paths()
{
	print("Checking paths....");
	if( !GLib.FileUtils.test(CFG_DIR, FileTest.IS_DIR) )
	{	
		print("Creating user data directory\n");
		GLib.DirUtils.create(CFG_DIR, 0755);
	}
	
	if( !GLib.FileUtils.test(DATA_DIR, FileTest.IS_DIR) )
	{	
		print("Creating user config directory\n");
		GLib.DirUtils.create(DATA_DIR, 0755);
	}
}
int main(string[] args)
{
	assert(GLib.Module.supported());
	string current_dir = GLib.Environment.get_current_dir();
	DS 				= GLib.Path.DIR_SEPARATOR.to_string();
	BASE_DIR		= current_dir;
	BIN_DIR			= current_dir;
	API_BASE 		= "http://localhost/SBFramework/api";
	CFG_DIR			= GLib.Environment.get_user_config_dir();
	DATA_DIR		= format_path(CFG_DIR + "/tl");
	LOG_FILENAME	= format_path(DATA_DIR + "/timerlogs.log");
	RUNTIME_DIR		= GLib.Environment.get_user_runtime_dir();
	
	#if __RELEASE__
	
	#if __MACOS__
	current_dir = GLib.Path.get_dirname(args[0]);
	BASE_DIR 	= GLib.Path.get_dirname(current_dir);
	BIN_DIR		= current_dir;
	#endif
	
	#if __WIN32__
	BASE_DIR 	= GLib.Path.get_dirname(current_dir);
	#endif
	
	API_BASE 	= "https://panel.timerlogs.com/api";
	
	#endif
	
	tl_check_paths();
	/*
	foreach(string arg in args)
	{
		tl_log(@"ARG: $arg\n");
	}
	*/
	#if __RELEASE__
	tl_log("TimerLogs Mode: RELEASE MODE\n");
	#endif
	tl_log("RUNTIME_DIR: %s\n".printf(RUNTIME_DIR));
	tl_log("CFG_DIR: %s".printf(CFG_DIR));
	tl_log("Working directory: %s\n".printf(current_dir));
	tl_log("BASEDIR: %s\n".printf(BASE_DIR));
	tl_log("BIN_DIR: %s\n".printf(BIN_DIR));
	tl_log("DATA_DIR: %s".printf(DATA_DIR));
	tl_log("LOG_FILENAME: %s".printf(LOG_FILENAME));
	
	load_config();
	
	Gtk.init(ref args);
	var window = new MainWindow();
	load_styles(window);
	var statusIcon = build_status_icon(window);
	window.window_position = WindowPosition.CENTER;
	window.set_default_size(800, 600);
	window.window_state_event.connect( (event) => 
	{
		if( Gdk.WindowState.FOCUSED == event.new_window_state )
		{
			tl_log("WINDOW STATE: FOCUSED");
			#if __WIN32__
			if( msg_h_added == 0 )
			{
				msg_h_added = 1;
				
				setMessageWindowHandler((void*)window.get_window());
			}
			#endif
			//stdout.printf("WINDOW FOCUS\n");
			window.on_window_focus();
		}
		if( Gdk.WindowState.WITHDRAWN == event.new_window_state )
		{
			window.get_application().hold();
			tl_log("WINDOW STATE: WITHDRAWN");
		}
		if( Gdk.WindowState.ICONIFIED == event.new_window_state )
		{
			window.get_application().hold();
			tl_log("WINDOW STATE: ICONOFIED/MINIMIZED");
		}
		//stdout.printf("STATE EVENT: %u, %u\n", event.type, event.new_window_state);
		 if(event.changed_mask == Gdk.WindowState.ICONIFIED && (event.new_window_state == Gdk.WindowState.ICONIFIED 
			|| event.new_window_state == (Gdk.WindowState.ICONIFIED | Gdk.WindowState.MAXIMIZED) ) )
		{
			//window.hide();
			window.get_application().hold();
			tl_log("WINDOW STATE: ICONIFIED");
			//statusIcon.visible = true;
		}
		else if(event.changed_mask == Gdk.WindowState.WITHDRAWN && (event.new_window_state == Gdk.WindowState.ICONIFIED 
			|| event.new_window_state == (Gdk.WindowState.ICONIFIED | Gdk.WindowState.MAXIMIZED)))
		{
			tl_log("WINDOW STATE: MAXIMIZED");
			//statusIcon.visible = false;
		}
		return true;
	});
	window.destroy.connect(() => 
	{
		window.ShutdownBeforeClose();
		Gtk.main_quit();
	});
	window.show_all();
	window.ValidateSession();
	window.Start();
	Gtk.main();
	return 0;
}
StatusIcon? build_status_icon(Window win)
{
	StatusIcon? statusIcon = null;
	
	try
	{
		statusIcon = new StatusIcon.from_pixbuf(
			new Gdk.Pixbuf.from_file_at_scale(tl_get_resource_path("images/logo-48x48.png"), 16, 16, true)
		);
		statusIcon.tooltip_text = "TimerLogs en ejecución";
		statusIcon.visible = true;
		statusIcon.activate.connect( () => 
		{
			win.show();
			win.deiconify();
		});
		statusIcon.popup_menu.connect( () => 
		{
			tl_log("Status icon popup menu\n");
		});
	}
	catch(GLib.Error e)
	{
		tl_log("ERROR: $(e.messages	)");
	}

	return statusIcon;
}
string format_path(string path)
{
	return path.replace("/", DS);
}
void load_styles(Window win)
{
	try
	{
		//##set css file
		var css = new CssProvider();
		//css.load_from_path (format_path(BASE_DIR + "/share/css/style.css"));
		css.load_from_path (tl_get_resource_path("css/style.css"));
		//Gdk.Display display = win.get_display ();
		Gdk.Screen screen = win.get_screen ();
		StyleContext.add_provider_for_screen(screen, css, 600);
	}
	catch(GLib.Error e)
	{
		tl_log("ERROR: Error loading styles\n%s\n" + e.message);
	}
}
void load_config()
{
	string kfilename = format_path(DATA_DIR + "/data.key");
	
	try
	{
		if( !GLib.FileUtils.test(kfilename, FileTest.EXISTS) )
		{
			GLib.FileUtils.set_contents(kfilename, 
"""
[Authentication]
jwt=
[Settings]
api_url=%s
last_company=0
[Preferences]
launch_at_login=true
activity_bar_visible=false
""".printf(API_BASE));
		}
		KeyFile kfile = new KeyFile();
		kfile.load_from_file(kfilename, KeyFileFlags.NONE);
		string jwt = kfile.get_string("Authentication", "jwt");
		if( jwt.strip().length <= 0 )
		{
			tl_log("Invalid JWT or not authenticated\n");
		}
		SBGlobals.SetValue("kfile", kfile);
	}
	catch(KeyFileError e)
	{
		tl_log("KEYFILE ERROR: " + e.message);
	}
	catch(FileError e)
	{
		tl_log("FILE ERROR: " + e.message);
	}
	
}
void save_config()
{
	try
	{
		string kfilename 	= format_path(DATA_DIR + "/data.key");
		var kfile 			= (KeyFile)SBGlobals.GetValue("kfile");
		kfile.save_to_file(kfilename);
	}
	catch(FileError e)
	{
		tl_log("ERROR: Ocurrio un error al guardar la configuracion\n");
	}
	
}
string tl_get_resource_path(string filename)
{
	#if __RELEASE__ && __MACOS__
	return BASE_DIR + "/Resources/" + filename;
	#else
	return format_path(BASE_DIR + "/share/" + filename);
	#endif
}
void tl_log(string str)
{
	try
	{
		var time = new DateTime.now_local();
		File file = File.new_for_path(LOG_FILENAME);
		var os = file.append_to(FileCreateFlags.NONE);
		os.write("[%s]:\n%s\n".printf(
			time.format("%Y-%m-%d %H:%M:%S"), 
			str).data
		);
		print(str + "\n");
	}
	catch(FileError e)
	{
		tl_log("ERROR: %s".printf(e.message));
	}
	catch(IOError e)
	{
		tl_log("IO ERROR: %s".printf(e.message));
	}
	catch(GLib.Error e)
	{
		tl_log("ERROR: %s".printf(e.message));
	}
}
