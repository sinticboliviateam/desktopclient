namespace TimerLogs
{
	
	public class OperatingSystem
	{
		public string OS;
		public string Name;
		public string Version;

		public OperatingSystem()
		{
			#if __WIN32__
			this.OS = "Win32";
			#elif __WIN64__
			this.OS = "Win32";
			#elif __linux__
			this.OS = "Linux";
			#elif __osx__
			this.OS = "MAC OS X";
			#elif __MACOS__
			this.OS = "MAC OS";
			#else
			this.OS = "Unknow";
			#endif
		}
		public bool IsWindows()
		{
			return (this.OS == "Win32" || this.OS == "Win32");
		}
		public bool IsLinux()
		{
			return this.OS == "Linux";
		}
		public bool IsUnix()
		{
			return (this.OS == "Linux" || this.OS == "Unix");
		}
		public bool IsOSX()
		{
			return (this.OS == "MAC OS X" || this.OS == "MAC OS");
		}
		public static OperatingSystem GetOS()
		{
			return new OperatingSystem();
		}
		public static string getConfigDir()
		{
			return GLib.Environment.get_user_config_dir();
		}
	}
}
