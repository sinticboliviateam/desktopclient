[CCode (cheader_filename = "KeyboardMouseData.h")]
namespace HooksData
{
	[CCode (cname = "HookApplicationData", free_function = "HookApplicationData_free"/*, has_type_id = false*/)]
	[Compact]
	public class VHookApplicationData
	{
		[CCode (cname = "HookApplicationData_new")]
		public VHookApplicationData();
		
		//[CCode (cname = "HookApplicationData_free")]
		//public void vfree();
		
		public string			name;
		public string			filename;
		public string			process;
		public ulong			timestamp;
	}
	
	[CCode (cname = "HookKeyboardMouseData", free_function = "HookKeyboardMouseData_free"/*, has_type_id = false*/)]
	[Compact]
	public class VHookKeyboardMouseData
	{
		[CCode (cname = "HookKeyboardMouseData_new")]
		public VHookKeyboardMouseData();
		
		//[CCode (cname = "HookKeyboardMouseData_free")]
		//public void vfree();
		
		public ulong 					clicks;
		public ulong					leftClicks;
		public ulong					rightClicks;
		public ulong					keydowns;
		public ulong					keyboardInactivity;
		public ulong					mouseInactivity;
		public ulong					inactivity;
		[CCode (array_null_terminated = true)]
		public VHookApplicationData[]	applications;
		
	}
}
