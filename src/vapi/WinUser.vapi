public const uint32 SRCCOPY;
public const uint32 CAPTUREBLT;
	
[CCode (cheader_filename = "windows.h")]
namespace Windows
{
	namespace Gdi
	{
		[CCode (cname = "BITMAP", has_type_id = false)]
		//[SimpleType]
		public struct Bitmap
		{
			public long bmType;
			public long bmWidth;
			public long bmHeight;
			public long bmWidthBytes;
			public uint16 bmPlanes;
			public uint16 bmBitsPixel;
			public void* bmBits;
			//uint8[] bmBits;
		}
		
		[CCode (cname = "BITMAPINFOHEADER", has_type_id = false)]
		public struct BitmapInfoHeader
		{
			ulong biSize;
			long biWidth;
			long biHeight;
			uint16 biPlanes;
			uint16 biBitCount;
			ulong biCompression;
			ulong biSizeImage;
			long biXPelsPerMeter;
			long biYPelsPerMeter;
			ulong biClrUsed;
			ulong biClrImportant;
		}
		
		[CCode (cname="RGBQUAD", has_type_id = false)]
		public struct RgbQuad
		{
			uint8	rgbBlue;
			uint8	rgbGreen;
			uint8	rgbRed;
			uint8	rgbReserved;
		}
		
		[CCode (cname = "BITMAPINFO", has_type_id = false)]
		public struct BitmapInfo
		{
			BitmapInfoHeader bmiHeader;
			RgbQuad bmiColors[1];
		}
		
		[CCode (cname = "GetObject")]
		public int GetObject(void* h,  int c, out Bitmap pv);
		
		[CCode (cname = "GetDIBits")]
		public int GetDIBits(void* hdc, void* hbm, uint start, uint cLines, void* lpvBits, ref BitmapInfo lpbmi, uint usage);
	}
}

[CCode (cheader_filename = "windows.h")]
namespace WinUser
{
	[CCode (cname = "int", cprefix = "SM_", has_type_id = false)]
	public enum SystemMetrics
	{
		CXSCREEN,
		CYSCREEN,
		CXVSCROLL,
		CYHSCROLL,
		CYCAPTION,
		CXBORDER,
		CYBORDER,
		CXDLGFRAME,
		CYDLGFRAME,
		CYVTHUMB,
		CXHTHUMB,
		CXICON,
		CYICON,
		CXCURSOR,
		CYCURSOR,
		CYMENU,
		CXFULLSCREEN,
		CYFULLSCREEN,
		CYKANJIWINDOW,
		MOUSEPRESENT,
		CYVSCROLL,
		CXHSCROLL,
		DEBUG,//                22
		SWAPBUTTON,
		RESERVED1,
		RESERVED2,
		RESERVED3,
		RESERVED4,
		CXMIN,
		CYMIN,
		CXSIZE,
		CYSIZE,
		CXFRAME,
		CYFRAME,
		CXMINTRACK,
		CYMINTRACK,
		CXDOUBLECLK,
		CYDOUBLECLK ,
		CXICONSPACING,
		CYICONSPACING,
		MENUDROPALIGNMENT ,
		PENWINDOWS, //           = 41,
		DBCSENABLED, //          = 42,
		CMOUSEBUTTONS, //        = 43,

//## #if(WINVER >= 0x0400)
		//CXFIXEDFRAME           CXDLGFRAME  /* ;win40 name change */
		//CYFIXEDFRAME           CYDLGFRAME  /* ;win40 name change */
		//CXSIZEFRAME            CXFRAME     /* ;win40 name change */
		//CYSIZEFRAME            CYFRAME     /* ;win40 name change */

		SECURE,//               44
		CXEDGE, //               45
		CYEDGE, //               46
		CXMINSPACING, //         47
		CYMINSPACING, //         48
		CXSMICON, //             49
		CYSMICON, //             50
		CYSMCAPTION, //          51
		CXSMSIZE, //             52
		CYSMSIZE, //             53
		CXMENUSIZE, //           54
		CYMENUSIZE, //           55
		ARRANGE, //              56
		CXMINIMIZED, //          57
		CYMINIMIZED, //          58
		CXMAXTRACK, //           59
		CYMAXTRACK, //           60
		CXMAXIMIZED, //          61
		CYMAXIMIZED, //          62
		NETWORK, //              63
		CLEANBOOT, //            67
		CXDRAG, //               68
		CYDRAG, //               69
//## #endif /* WINVER >= 0x0400 */
		SHOWSOUNDS, //           70
//## #if(WINVER >= 0x0400)
		 CXMENUCHECK, //          71   /* Use instead of GetMenuCheckMarkDimensions()! */
		 CYMENUCHECK, //          72
		 SLOWMACHINE, //          73
		 MIDEASTENABLED, //       74
//## #endif /* WINVER >= 0x0400 */

//## #if (WINVER >= 0x0500) || (_WIN32_WINNT >= 0x0400)
		MOUSEWHEELPRESENT, //    75
//## #endif

	}
	[CCode (cname = "GetDesktopWindow")]
	public void* GetDesktopWindow();
	
	[CCode (cname = "GetSystemMetrics")]
	public int GetSystemMetrics(int metric);
	
	[CCode (cname = "GetDC")]
	public void* GetDC(void * hWnd);
	
	[CCode (cname = "ReleaseDC")]
	public void ReleaseDC(void* hDesktopWnd, void* hDesktopDC);
	
	[CCode (cname = "DeleteDC")]
	public void DeleteDC(void* hCaptureDC);
	
	[CCode (cname = "DeleteObject")]
	public void DeleteObject(void* hBitmap);
	
	[CCode (cname = "CreateCompatibleDC")]
	public void* CreateCompatibleDC(void* hDesktopDC); 
	
	[CCode (cname = "CreateCompatibleBitmap")]
	public void* CreateCompatibleBitmap(void* hDesktopDC, int width, int height); 
	
	[CCode (cname = "SelectObject")]
	public void* SelectObject(void* hCaptureDC, void* hCaptureBMP);
	
	[CCode (cname = "BitBlt")]
	public bool BitBlt(void* hdc, int x, int y, int cx, int cy, void* hdcSrc, int x1, int y1, uint32 rop);
}
