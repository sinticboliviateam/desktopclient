[CCode (cheader_filename = "capture.h")]
namespace TimerLogs.Windows.Classes
{
	[CCode (cname = "WinApplicationData")]
	public struct WinApplicationData
	{
		public string	applicationName;
		public string	processName;
		public string	moduleName;
		public string	operatingSystem;
	}
}

