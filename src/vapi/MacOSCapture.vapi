[CCode (cheader_filename = "capture-macos.h")]
namespace MacOSCapture
{
	[CCode (cname = "tl_set_log_file")]
	void tl_set_log_file(char* filename);
	
	[CCode (cname = "capture_desktop")]
	void capture_desktop(string filename);
	
	[CCode (cname = "fast_capture_desktop")]
	void fast_capture_desktop(string destination);
	
	[CCode (cname = "check_accesibility_access")]
	bool check_accesibility_access();
	
	[CCode (cname = "check_capture_screen_access")]
	bool check_capture_screen_access();
	
	[CCode (cname = "open_accesibility")]
	void open_accesibility();
	
	[CCode (cname = "start_km_capture")]
	void start_km_capture();
	
	[CCode (cname = "stop_km_capture")]
	void stop_km_capture();
	
	[CCode (cname = "getData")]
	HooksData.VHookKeyboardMouseData getData();
	
	[CCode (cname = "getActiveWindowData")]
	HooksData.VHookApplicationData getActiveWindowData();
	
	[CCode (cname = "HookApplicationData_new")]
	HooksData.VHookApplicationData HookApplicationData_new();
	
	[CCode (cname = "HookApplicationData_free")]
	void HookApplicationData_free(HooksData.VHookApplicationData obj);
}
