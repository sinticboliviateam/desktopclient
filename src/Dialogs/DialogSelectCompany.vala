using Gtk;
using TimerLogs.Models;
using Gee;

namespace TimerLogs.Dialogs
{
	public class DialogSelectCompany : Dialog
	{
		protected	Button		btnCancel;
		protected	ListBox		listBoxCompanies;
		protected	unowned Company		companySelected;
		
		public DialogSelectCompany()
		{
			this.window_position	= WindowPosition.CENTER;
			this.btnCancel 			= (Button)this.add_button("Cancelar", 0);
			this.btnCancel.get_style_context().add_class("btn");
			this.btnCancel.get_style_context().add_class("btn-danger");
			(this.btnCancel.parent as Box).set_child_packing(this.btnCancel, true, true, 5, PackType.START);
			this.listBoxCompanies 	= new ListBox();
			this.listBoxCompanies.row_activated.connect(this.OnCompanySelected);
			this.set_default_size(350, 350);
			var labelDescription = new Label("Selecciona la compañia con la que deseas empezar a trabajar");
			labelDescription.xalign = 0.0f;
			labelDescription.show();
			this.get_content_area().pack_start(labelDescription, false, true, 8);
		}
		public void SetCompanies(ArrayList<Company> companies)
		{
			print("SetCompanies...");
			companies.foreach((company) =>
			{
				var box = new Box(Orientation.HORIZONTAL, 5);
				var boxData = new Box(Orientation.VERTICAL, 3);
				
				boxData.pack_start(new Label(company.name));
				
				box.pack_start(boxData, true, true);
				box.pack_start(
					new Image.from_file(tl_get_resource_path("images/right-arrow-16.png")), 
					false, 
					true
				);
				
				var listBoxRow = new ListBoxRow();
				listBoxRow.activatable 	= true;
				listBoxRow.selectable	= true;
				listBoxRow.set_data<Company>("company", company);
				listBoxRow.get_style_context().add_class("listbox-row-item");
				listBoxRow.add(box);

				this.listBoxCompanies.insert(listBoxRow, -1);
				return true;
			});
			print("[DONE]\n");
			this.listBoxCompanies.show_all();
			this.get_content_area().pack_start(this.listBoxCompanies);
		}
		protected void OnCompanySelected(ListBoxRow row)
		{
			unowned var company = row.get_data<Company>("company");
			print("Company selected: %s\n", company.name);
			this.companySelected = company;
			this.response(1);
		}
		public unowned Company GetCompanySelected()
		{
			return this.companySelected;
		}
	}
}
