namespace TimerLogs.Classes
{
	public class ApiResponse
	{
		public		uint			http_code = 0;
		public		string 			response = "";
		protected	Json.Object?	obj;
		
		public ApiResponse(string json, uint code)
		{
			this.response = json;
			this.http_code = code;
			this.parseJson();
		}
		protected void parseJson()
		{
			if( this.response.strip().length <= 0 )
				return;
			
			var parser = new Json.Parser();
			
			try
			{
				if( !parser.load_from_data(this.response, -1) )
					return;
				this.obj = parser.get_root().get_object();
			}
			catch(GLib.Error e)
			{
				print(e.message);
			}
		}
		public Json.Object? getObject(string obj_name = "")
		{
			return this.obj;
		}
		public Json.Object? getObjectData()
		{
			if( this.obj == null )
				return null;
				
			var odata = this.obj.get_object_member("data");
			
			return odata;
		}
		public Json.Array? getArrayData()
		{
			if( this.obj == null )
				return null;
				
			var adata = this.obj.get_array_member("data");
			
			return adata;
		}
		public Json.Node? getMemberData()
		{
			if( this.obj == null )
				return null;
			var m = this.obj.get_member("data");
			
			return m;
		}
		public bool hasError()
		{
			if( this.obj == null )
				return false;
				
			return this.obj.has_member("error");
		}
		public string getError()
		{
			if( !this.hasError() )
				return "";
			return this.obj.get_string_member("error");
		}
	}
}
