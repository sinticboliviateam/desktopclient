using Gtk;
using WinUser;
using Gdk;
using HooksData;

//using WinHooks;

public extern int CaptureAnImage(void* hWnd, char* filename);
public extern void* getActiveWindowData();

namespace TimerLogs.Classes
{
	public class LoggerWin : Object, ILogger
	{
		protected	delegate void 	InstallHook();
		protected	delegate void 	UninstallHook();
		protected	delegate void*	GetHookData();
		
		
		protected GLib.Module	dll;
		
		public LoggerWin()
		{
			this.loadDll();
			this.loadSymbols();
		}
		public void loadDll()
		{
			//##load hooks
			string hook_path = GLib.Module.build_path(BIN_DIR, "libwinhooks.dll");
			tl_log(@"Hook DLL path: $(hook_path)\n");
			this.dll = GLib.Module.open(hook_path, ModuleFlags.LAZY);
			if(this.dll == null)
			{
				tl_log("Unable to load HOOKS DLL\n");
				return;
			}
			this.dll.make_resident();
		}
		public void loadSymbols()
		{
		}
		public virtual bool askPermissions()
		{
		}
		public virtual void startKMCapture()
		{
			tl_log("STARTING UP WIN HOOKS\n");
			void* InstallFunction;
			if( !this.dll.symbol("install", out InstallFunction) )
			{
				tl_log("Unable to get hook install symbol\n");
				return;
			}
			unowned InstallHook ihook = (InstallHook)InstallFunction;
			ihook();
		}
		public virtual void stopKMCapture()
		{
			tl_log("SHUTTING DOWN WIN HOOKS");
			void* UninstallFunction;
			
			if( !this.dll.symbol("uninstall", out UninstallFunction) )
			{
				tl_log("Unable to get hook uninstall symbol\n");
				return;
			}
			unowned UninstallHook uihook 	= (UninstallHook)UninstallFunction;
			uihook();
		}
		public virtual void captureDesktopToBuffer(out uint8[] buffer)
		{
			buffer = null;
			//int nScreenWidth	= GetSystemMetrics(WinUser.SystemMetrics.CXSCREEN);
			//int nScreenHeight	= GetSystemMetrics(WinUser.SystemMetrics.CYSCREEN);
			void* hDesktopWnd	= GetDesktopWindow();
			//tl_log("Metrics: %dx%d\nSRCCOPY: %u\nCAPTUREBLT: %u\n".printf(nScreenWidth, nScreenHeight, SRCCOPY, CAPTUREBLT));
			
			string filename = format_path(DATA_DIR + "/screenshot.bmp");
			CaptureAnImage(hDesktopWnd, filename.to_utf8());
			try
			{
				Pixbuf screenshot = new Pixbuf.from_file(filename);
				//remove BMP
				if( FileUtils.test(filename, FileTest.IS_REGULAR) )
				{
					FileUtils.remove(filename);
				}
				screenshot.save_to_buffer(out buffer, "jpeg");
			}
			catch(GLib.Error e)
			{
				tl_log(@"ERROR Ocurrio un error al tratar de guardar la captura $(e.message)\n");
			}
		}
		public virtual AppData getCurrentAppData()
		{
			AppData appData = new AppData();
			
			//WinApplicationData* data = (WinApplicationData*)getActiveWindowData();
			//TimerLogs.Windows.Classes.WinApplicationData* data = (TimerLogs.Windows.Classes.WinApplicationData*)getActiveWindowData();
			unowned VHookApplicationData? data = (VHookApplicationData)getActiveWindowData();
			if( data == null )
			{
				tl_log("INVALID APPDATA\n");
				return appData;
			}
			appData.applicationName = data.name.normalize();
			appData.processName		= data.process.normalize();
			appData.moduleName		= data.filename.normalize();
			appData.timestamp		= data.timestamp;
			appData.operatingSystem	= "Windows";
			
			//delete data;
			return appData;
		}
		public virtual KeyboardMouseData captureKMData()
		{
			KeyboardMouseData kmData 	= new KeyboardMouseData();
			tl_log("GETTING WIN HOOKS DATA\n");
			void* function;
			if( !this.dll.symbol("getData", out function) )
			{
				tl_log("Unable to get hook getData symbol\n");
				return kmData;
			}
			unowned GetHookData getData = (GetHookData)function;
			unowned VHookKeyboardMouseData wkmData = (VHookKeyboardMouseData)getData();
			
			kmData.clicks 				= wkmData.clicks;
			kmData.leftClicks			= wkmData.leftClicks;
			kmData.rightClicks			= wkmData.rightClicks;
			kmData.keydowns				= wkmData.keydowns;
			kmData.keyboardInactivity	= wkmData.keyboardInactivity;
			kmData.mouseInactivity		= wkmData.mouseInactivity;
			kmData.inactivity			= wkmData.inactivity;
			
			for(int i = 0; i < wkmData.applications.length; i++)
			{
				unowned VHookApplicationData? ad = (VHookApplicationData)wkmData.applications[i];
				if( ad == null ) continue;
				var app				= new AppData();
				app.applicationName = ad.name.normalize();
				app.processName		= ad.process.normalize();
				/*
				unichar c;
				for (int j = 0; ad.process.get_next_char (ref j, out c);) {
					//stdout.printf ("%d, %s\n", i, c.to_string ());
					//app.processNane += "%s".printf((string)ad.process.data);
					app.processName += "%s".printf(c.to_string());
				}
				*/
				app.moduleName		= ad.filename.normalize();
				app.timestamp		= ad.timestamp;
				kmData.apps.add(app);
				tl_log("Application name: %s\nProcess Name: %s\nModule name: %s\n".printf(
					app.applicationName,
					app.processName,
					app.moduleName
					
				));
			}
						
			return kmData;
		}
	}
}
