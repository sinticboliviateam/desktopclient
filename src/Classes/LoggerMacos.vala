using Gdk;
using HooksData;

namespace TimerLogs.Classes
{
	public class LoggerMacos : Object, ILogger
	{
		protected	GLib.Module		lib;
		
		protected	bool	stopKMCaptureThread {get;set;}
		protected	bool	accesibilityPermission = false;
		protected	bool	screenPermission = false;
		
		public LoggerMacos()
		{
			//this.loadLib();
			//this.loadSymbols();
			MacOSCapture.tl_set_log_file(LOG_FILENAME);
		}
		/*
		protected void loadLib()
		{
			//string filename = format_path("%s/libcapture.dylib".sprintf(BIN_DIR));
			//##load hooks
			string hook_path = GLib.Module.build_path(BIN_DIR, "libcapture.dylib");
			tl_log("Capture MACOS dylib path: %s\n".printf(hook_path));
			this.lib = GLib.Module.open(hook_path, ModuleFlags.LAZY);
			if(this.lib == null)
			{
				tl_log("Unable to load MACOS CAPTURE dylib\n");
				return;
			}
			this.lib.make_resident();
		}
		/*
		protected void loadSymbols()
		{
			void* function;
			
			if( !this.lib.symbol("check_accesibility_access", out function) )
			{
				tl_log("Unable to get \"check_accesibility_access\" symbol\n");
			}
			this.check_accesibility_access = (delegate_ask_permissions)function;
			tl_log("Symbol check_accesibility_access...	[DONE]");
			
			
			if( !this.lib.symbol("open_accesibility", out function) )
			{
				tl_log("Unable to get \"open_accesibility\" symbol\n");
				return;
			}
			this.open_accesibility = (delegate_open_accesibility)function;
			tl_log("Symbol open_accesibility...	[DONE]");
			
			if( !this.lib.symbol("start_km_capture", out function) )
			{
				tl_log("Unable to get start_km_capture symbol\n");
				return;
			}
			this.start_km_capture = (delegate_start_km_capture)function;
			tl_log("symbol start_km_capture: [DONE]");

			if( !this.lib.symbol("stop_km_capture", out function) )
			{
				tl_log("Unable to get stop_km_capture symbol\n");
				return;
			}
			this.stop_km_capture = (delegate_stop_km_capture)function;
			tl_log("symbol stop_km_capture [DONE]");
			
			
			if( !this.lib.symbol("getData", out function) )
			{
				tl_log("Unable to get hook \"getData\" symbol\n");
				return;
			}
			this.getData = (delegate_getData)function;
			tl_log("symbol getData: [DONE]");
			
			if( !this.lib.symbol("HookKeyboardMouseData_free", out function) )
			{
				tl_log("Unable to get hook \"HookKeyboardMouseData_free\" symbol\n");
				return ;
			}
			this.free_kmdata = (delegate_free_kmdata)function;
			tl_log("Symbol HookKeyboardMouseData_free...	[DONE]");
			

			if( !this.lib.symbol("fast_capture_desktop", out function) )
			{
				tl_log("ERROR: Unable to get fast_capture_desktop symbol\n");
				return;
			}
			this.fast_capture_desktop = (delegate_fast_capture_desktop)function;
			tl_log("Symbol fast_capture_desktop...	[DONE]");

			if( !this.lib.symbol("getActiveWindowData", out function) )
			{
				tl_log("Unable to get getActiveWindowData symbol\n");
				return;
			}
			this.getApplicationData = (delegate_getApplicationData)function;
			tl_log("Symbol getActiveWindowData...	[DONE]");
			
			if( !this.lib.symbol("HookApplicationData_free", out function) )
			{
				tl_log("Unable to get HookApplicationData_free symbol\n");
				return;
			}
			this.free_appdata = (delegate_free_appdata)function;
			tl_log("Symbol HookApplicationData_free...	[DONE]");
			
			if( !this.lib.symbol("tl_set_log_file", out function) )
			{
				tl_log("Unable to get HookApplicationData_free symbol\n");
				return;
			}
			var tl_set_log_file = (delegate_tl_set_log_file)function;
			tl_set_log_file(LOG_FILENAME);
			tl_log("Symbols loaded...	[DONE]");
		}
		*/
		protected bool check_accesibility_access()
		{
			return MacOSCapture.check_accesibility_access();
		}
		protected HooksData.VHookApplicationData getApplicationData()
		{
			return MacOSCapture.getActiveWindowData();
		}
		protected HooksData.VHookKeyboardMouseData getData()
		{
			return MacOSCapture.getData();
		}
		protected void start_km_capture()
		{
			MacOSCapture.start_km_capture();
		}
		protected void stop_km_capture()
		{
			MacOSCapture.stop_km_capture();
		}
		protected void free_appdata(HooksData.VHookApplicationData obj)
		{
			MacOSCapture.HookApplicationData_free(obj);
		}
		protected void open_accesibility()
		{
			MacOSCapture.open_accesibility();
		}
		protected void fast_capture_desktop(string filename)
		{
			MacOSCapture.fast_capture_desktop(filename);
		}
		public virtual bool askPermissions()
		{
			string msg = "Checking accesibility permissions...";
			this.accesibilityPermission = (bool)this.check_accesibility_access();
			if( !this.accesibilityPermission )
			{
				msg += "[ERROR]\n-> We need to show accesbility access permissions.";
			}
			else
			{
				msg += "[DONE]";
			}
			tl_log(msg);
			
			msg = "Checking screen recording permissions ...";
			this.screenPermission = MacOSCapture.check_capture_screen_access();
			if( !this.screenPermission )
			{
				msg += "[ERROR]\n-> We need to show screen recording access permissions.";
			}
			else
				msg += "[DONE]";
			tl_log(msg);
			
			return this.accesibilityPermission && this.screenPermission;
		}
		public virtual void startKMCapture()
		{
			ThreadFunc<void> cTask = () => 
			{
				this.start_km_capture();
				tl_log("start_km_capture [DONE]");
			};
			new Thread<void>.try("thread_macos_km_capture", (owned)cTask);
		}
		public virtual void stopKMCapture()
		{
			this.stop_km_capture();
		}
		public virtual KeyboardMouseData captureKMData()
		{
			tl_log("Collecting data: captureKMData");
			KeyboardMouseData kmData 	= new KeyboardMouseData();
			
			//unowned VHookKeyboardMouseData? hkmData = (VHookKeyboardMouseData)this.getData();
			VHookKeyboardMouseData? hkmData = this.getData();
			if( hkmData == null )
			{	
				tl_log("Keyboard mouse data is null");
				return kmData;
			}	
			kmData.clicks 				= hkmData.clicks;
			kmData.leftClicks			= hkmData.leftClicks;
			kmData.rightClicks			= hkmData.rightClicks;
			kmData.keydowns				= hkmData.keydowns;
			kmData.keyboardInactivity	= hkmData.keyboardInactivity;
			kmData.mouseInactivity		= hkmData.mouseInactivity;
			kmData.inactivity			= hkmData.inactivity;
			
			for(int i = 0; i < hkmData.applications.length; i++)
			{
				unowned HooksData.VHookApplicationData? ad = (HooksData.VHookApplicationData)hkmData.applications[i];
				if( ad == null ) continue;
				var app				= new AppData();
				app.applicationName = ad.name.normalize();
				app.processName		= ad.process.normalize();
				app.moduleName		= ad.filename.normalize();
				app.timestamp		= ad.timestamp;
				kmData.apps.add(app);
			}
			tl_log("KeyboardMouseData collected");
			//##release de object from objective-c
			//---->this.free_kmdata(hkmData);
			//tl_log("Dealloc raw keyboardMouseData");
			return kmData;
		}
		public string captureDesktop()
		{
			string destination = format_path(DATA_DIR + "/screenshot.jpg");
			this.fast_capture_desktop(destination);
			
			tl_log("FAST_CAPTURE_DESKTOP [DONE]\n");
			return destination;
		}
		public virtual void captureDesktopToBuffer(out uint8[] buffer)
		{
			buffer = null;
			try
			{
				string filename = this.captureDesktop();
				if( FileUtils.test(filename, FileTest.IS_REGULAR) )
				{
					Pixbuf screenshot = new Pixbuf.from_file(filename);
					screenshot.save_to_buffer(out buffer, "jpeg");
					tl_log("Deleting screenshot file: %s\n".printf(filename));
					//remove image
					FileUtils.remove(filename);
				}
				else
				{
					tl_log("ERROR: the screenshot does not exists: %s\n".printf(filename));
				}
			}
			catch(GLib.Error e)
			{
				tl_log("Ocurrio un error al tratar de guardar la captura\nERROR: %s".printf(e.message));
			}
		}
		public virtual AppData getCurrentAppData()
		{
			AppData appData = new AppData();
			
			//unowned HooksData.VHookApplicationData? rawAppData = (HooksData.VHookApplicationData)this.getApplicationData();
			HooksData.VHookApplicationData? rawAppData = this.getApplicationData();
			if( rawAppData == null )
			{
				tl_log("VHookApplicationData == NULL");
				return appData;
			}
			appData.applicationName = rawAppData.name.normalize();
			appData.processName		= rawAppData.process.normalize();
			appData.moduleName		= rawAppData.filename.normalize();
			appData.timestamp		= rawAppData.timestamp;
			appData.operatingSystem	= "MACOS";
			
			tl_log("delete rawAppDAta");
			//this.free_appdata(rawAppData);
			
			return appData;
		}
		/*
		public void CaptureForOSX()	
		{
			string output = "";
			string error = "";
			//string req_cmd = "/usr/bin/osascript -e 'tell application \"timerlogs\"' -e 'set _b to bounds of window of desktop' -e 'end tell'";
			string[] args = {
				"osascript",
				"-e 'tell application \"Finder\"'",
				"-e 'set _b to bounds of window of desktop'",
				"-e 'end tell'"
			};
			//GLib.Process.spawn_command_line_sync(req_cmd, out output, out error);
			try
			{
				string cwd = GLib.Environment.get_current_dir() + "/share/scripts";
				tl_log("Working directory: %s\n".printf(cwd));
				GLib.Process.spawn_sync(
					cwd, 
					{"./mac.scpt"}, 
					Environ.get(), 
					SpawnFlags.SEARCH_PATH, 
					null, 
					out output, 
					out error
				);
				print("COMMAND OUTPUT\n%s", output);
				print("COMMAND ERROR\n%s", error);
			}
			catch(GLib.SpawnError e)
			{
				print("MAC OSX ERROR: $(e.message)");
			}
			//string cmd = GLib.Environment.get_current_dir() + "/share/scripts/mac.scpt";
			//GLib.Process.spawn_command_line_sync(cmd, out output, out error);
		}
		*/
	}
}
