using GLib;
using TimerLogs.Models;

namespace TimerLogs.Classes
{
	public class UserData : Object
	{
		protected DateTime			datetime;
		public 	string				datetimeStr {get;set;}
		public 	int64 				timestamp 	{get;set;}
		public 	string				screenshot 	{get;set;}
		public 	int64				user_id 	{get;set;}
		public 	string				username 	{get;set;}
		public	int64				project_id	{get;set;}
		public	int64				task_id		{get;set;}
		public	string				ttype		{get;set;}
		public 	KeyboardMouseData	kmData 		{get;set;}
		public	TimeData			timeData	{get;set;}
		public	AppData				appData		{get;set;}
		public	TaskData			taskData	{get;set;}
		public	uint				seconds		{get;set;}
		
		public UserData()
		{
			this.datetime 	= new DateTime.now_local();
			this.screenshot = null;
			this.username 	= "test_user";
			this.kmData		= new KeyboardMouseData();
			this.timeData	= new TimeData();
			this.appData	= new AppData();
		}
		public void loadFromJson(string json)
		{
			try
			{
				var parser = new Json.Parser();
				parser.load_from_data(json, -1);
				var root_obj = parser.get_root().get_object();
				this.username = root_obj.get_string_member("username");
			}
			catch(GLib.Error e)
			{
				print("ERROR: " + e.message + "\n");
			}
		}
		public string toJson()
		{
			this.timestamp 		= this.datetime.to_unix();
			this.datetimeStr	= this.datetime.format("%Y-%m-%d %H:%M:%S");
			//## set time data
			this.timeData.hour		= this.datetime.get_hour();
			this.timeData.minute 	= this.datetime.get_minute();
			this.timeData.second	= this.datetime.get_second();
			this.timeData.timestamp	= this.timestamp;
			size_t length;
			string json = Json.gobject_to_data(this, out length);
			return json;
		}
	}
}
