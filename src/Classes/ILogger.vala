namespace TimerLogs.Classes
{
	public interface ILogger : GLib.Object
	{
		public abstract void 				startKMCapture();
		public abstract void 				stopKMCapture();
		public abstract KeyboardMouseData 	captureKMData();
		public abstract void 				captureDesktopToBuffer(out uint8[] buffer);
		public abstract AppData 			getCurrentAppData();
		public abstract bool				askPermissions();
	}
}
