namespace TimerLogs.Classes
{
	public class AppData : Object
	{
		public	string	applicationName {get;set;}
		public	string	processName 	{get;set;}
		public	string	moduleName 		{get;set;}
		public	string	operatingSystem	{get;set;}
		public	ulong	timestamp{get;set;}
		
		public AppData()
		{
		}
	}
}
