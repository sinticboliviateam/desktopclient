using GLib;
using Gee;

namespace TimerLogs.Classes
{
	public class KeyboardMouseData : Object, Json.Serializable
	{
		public ulong 				clicks 				{get;set;default = 0;}
		public ulong				leftClicks 			{get;set;default = 0;}
		public ulong				rightClicks 		{get;set;default = 0;}
		public ulong				keydowns 			{get;set;default = 0;}
		public double				keyboardInactivity	{get;set;default = 0;}
		public double				mouseInactivity		{get;set;default = 0;}
		public double				inactivity 			{get;set;default = 0;}
		
		public	ArrayList<AppData>	apps				{get;set;}

		public KeyboardMouseData()
		{
			this.apps = new ArrayList<AppData>();
		}
		public string toJson()
		{
			size_t length;
			string json = Json.gobject_to_data(this, out length);
			return json;
		}
		
		public virtual Json.Node serialize_property(string property_name, Value value, ParamSpec pspec)
		{
			//print(@"Serializing KeyboardMouseData.$property_name\n");
			if( property_name == "apps")
			{
				//var gen = new Json.Generator();
				var root = new Json.Node(Json.NodeType.ARRAY);
				var items = new Json.Array();
				//gen.set_root(root);
				root.set_array(items);
				foreach(var app in this.apps)
				{
					var nodeApp = Json.gobject_serialize(app);
					items.add_element(nodeApp);
				}
				
				return root;
			}
			return this.default_serialize_property(property_name, value, pspec);
		}
		public virtual unowned ParamSpec? find_property(string name)
		{
			print("find_property: $name\n");
			return ((ObjectClass) get_type ().class_ref ()).find_property (name);
		}
		
	}
}
