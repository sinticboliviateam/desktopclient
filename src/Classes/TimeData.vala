using TimerLogs.Models;

namespace TimerLogs.Classes
{
	public class TimeData : Object
	{
		public const	int HOUR_SECONDS 	= 3600;
		public const	int DAY_SECONDS		= 86400;
		
		public	uint	hour{get;set;default = 0;}
		public	uint	minute{get;set;default = 0;}
		public	uint	second{get;set;default = 0;}
		public	int64	timestamp{get;set;default = 0;}
		public	uint	totalSeconds{get;set;default = 0;}
		
		public TimeData()
		{
			this.hour 		= 0;
			this.minute 	= 0;
			this.second 	= 0;
			this.timestamp 	= 0;
			this.totalSeconds = 0;
		}
		public TimeData.fromTaskData(TaskData tdata)
		{
			this();
			this.setTimeFromSeconds((uint)tdata.task_accumulated_time);
		}
		public TimeData.fromSeconds(uint seconds)
		{
			this();
			this.setTimeFromSeconds(seconds);
		}
		public uint getTotalSeconds()
		{
			uint total_seconds = this.second;
			if( this.minute > 0 )
				total_seconds += this.minute * 60;
			if( this.hour > 0 )
				total_seconds += (this.hour * 60) * 60;
			
			return total_seconds;
		}
		public double getTotalMinutes()
		{
			uint total_seconds		= this.getTotalSeconds();
			double total_minutes 	= total_seconds / 60.0d;
			
			return total_minutes;
		}
		public double getTotalHours()
		{
			print("Hours: %u\nMinutes: %u\nSeconds: %u\n", this.hour, this.minute, this.second);
			uint total_seconds 		= this.getTotalSeconds();
			double total_minutes 	= this.getTotalMinutes();
			double total_hours 		= total_minutes / 60.0d;
			print(@"Total seconds: $total_seconds\nTotal minutes: %lf\nTotalHours: %lf\n", total_minutes, total_hours);
			return total_hours;
		}
		public void setTimeFromHours(double hours)
		{
			this.hour 				= (uint)GLib.Math.floor(hours);
			double hoursDecimal 	= (hours - this.hour);
			double minutes 			= hoursDecimal * 60.0d;
			double minutesDecimal 	= (minutes - GLib.Math.floor(minutes));
			this.minute				= (uint)GLib.Math.floor(minutes);
			this.second				= (uint)GLib.Math.floor(minutesDecimal * 60.0d);
			this.totalSeconds		= (uint)(hours * HOUR_SECONDS);
		}
		public void setTimeFromSeconds(uint seconds)
		{
			this.totalSeconds	= seconds;
			double hours 		= secondsToHours(seconds);
			this.setTimeFromHours(hours);
		}
		public static double secondsToHours(uint seconds)
		{
			double hours = ((double)seconds / (double)HOUR_SECONDS);
			//print("PARSING SECONDS2HOURS cf(%d): %u -> %lf\n", HOUR_SECONDS, seconds, hours);
			return hours;
		}
	}
}
