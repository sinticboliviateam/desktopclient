namespace TimerLogs.Exceptions
{
	errordomain Project
	{
		INVALID_PROJECT,
		INVALID_PROJECT_ID,
		INVALID_PROJECT_NAME
	}
	errordomain Task
	{
		INVALID_TASK,
		INVALID_TASK_ID,
		INVALID_TASK_NAME
	}
}
