using Gtk;
using TimerLogs.Widgets;
using TimerLogs.Models;
using TimerLogs.Classes;
using TimerLogs.Classes.SinticBolivia;
using TimerLogs.Api;
using TimerLogs.Models;
using Gee;

namespace TimerLogs
{
	public class MainWindow : Window
	{
		protected	Box	mainBox;
		protected	Box	leftBox;
		protected	Box	wrapContent;
		protected	Box	contentBox;
		protected	Box	topBox;
		protected	ScrolledWindow		contentScroll;
		
		protected	Button		buttonTimer;
		protected	Button		buttonSettings;
		protected	Widget		buttonAvatar;
		protected	Label		currentTask;
		protected	Label		currentProject;
		protected	Overlay		overlay;
		protected	Button		buttonStart;
		protected	Button		buttonPause;
		
		protected	TimerLogs.Models.Project	cProject;
		protected	TimerLogs.Models.Task		cTask;
		
		protected	WidgetLogin					widgetLogin;
		protected	TimerLogs.Widgets.Timer		widgetTimer;
		protected	Logger						logger;
		protected	User?						cUser;
		protected	Box							mainContainer;
		protected	Gee.ArrayList<Company>		companies;
		protected	unowned Company?			currentCompany;
		protected	Label						labelUsername;
		//protected	Label						labelCompanyName;
		protected	ListBoxRow					listBoxRowSwitchCompany;
		
		
		public signal void on_window_focus();
		
		public MainWindow()
		{
			//base();
			this.name = "main-window";
			try
			{
				this.icon = new Gdk.Pixbuf.from_file(tl_get_resource_path("images/logo-48x48.png"));
			}
			catch(GLib.Error e)
			{
				tl_log("WINDOW ICON ERROR: $(e.message)");
			}
			
			this.mainContainer = new Box(Orientation.VERTICAL, 0);
			this.add(mainContainer);
			this.labelUsername 			= new Label("Username");
			this.labelUsername.xalign 	= 0.0f;
			//this.labelCompanyName		= new Label("Your company");
			//this.labelCompanyName.xalign = 0.0f;
			this.BuildLogin();
			this.BuildMain();
			this.companies = new ArrayList<Company>();
			this.logger = new Logger(this, null);
			
			this.SetEvents();
		}
		protected void SetEvents()
		{
			this.on_window_focus.connect(this.OnWindowFocus);
			this.buttonSettings.clicked.connect( this.OnButtonSettingsClicked );
		}
		public void Start()
		{
			
		}
		protected void OnWindowFocus()
		{
			tl_log("REFRESH TIMER WIDGET\n");
			#if __MACOS__
			if( !this.logger.permissionsOk )
			{
				tl_log("MACOS: Invalid permissions, the application cant operate correctly");
			}
			#endif
			this.widgetTimer.updateWorkedTime();
		}
		public void BuildLogin()
		{
			this.widgetLogin = new WidgetLogin();
			this.widgetLogin.login_success.connect( this.OnLoginSuccess );
			this.mainContainer.add(this.widgetLogin);
		}
		public void BuildMain()
		{
			//var user = this.cUser = (User)SBGlobals.GetValue("user");
			//print("Username: %s\n", user.username);
			this.mainBox			= new Box(Orientation.HORIZONTAL, 0);
			this.leftBox 			= new Box(Orientation.VERTICAL, 1);
			this.wrapContent 		= new Box(Orientation.VERTICAL, 0);
			this.topBox				= new Box(Orientation.HORIZONTAL, 0);
			this.contentBox 		= new Box(Orientation.VERTICAL, 0);
			
			
			this.leftBox.name		= "left-sidebar";
			this.topBox.name		= "top-bar";
			this.topBox.homogeneous = true;
			this.wrapContent.name	= "wrap-content";
			this.wrapContent.pack_start(this.topBox, false, true);
			this.wrapContent.pack_start(this.contentBox, true, true);
			//##add sidebar
			this.mainBox.pack_start(this.leftBox, !true, false);
			//##add right content wrapper
			this.mainBox.pack_start(this.wrapContent, true, true);
			//this.contentScroll		= new ScrolledWindow(null, null	);
			//this.contentScroll.add(this.contentBox);//this.contentScroll.add_with_viewport(this.contentBox);
			//this.wrapContent.pack_start(this.contentScroll, true, true);
			this.overlay = new Overlay();
			this.overlay.add(this.mainBox);
			this.mainContainer.pack_start(overlay, true, true);
			
			this.BuildSidebar();
			this.BuildTopBar();
		}
		protected void BuildSidebar()
		{
			this.buttonTimer = new Button();
			this.buttonTimer.set_image(new Image.from_file(tl_get_resource_path("images/timer-24.png")));
			this.buttonTimer.has_tooltip = true;
			this.buttonTimer.tooltip_text = "Registro de tiempos";
			
			this.buttonSettings = new Button();
			this.buttonSettings.set_image(new Image.from_file(tl_get_resource_path("images/settings-5-24.png")));
			this.buttonSettings.has_tooltip = true;
			this.buttonSettings.tooltip_text = "Configuración";
			
			this.buttonAvatar = this.BuildAvatarButton();
			this.leftBox.pack_start(this.buttonAvatar, false, true);
			this.leftBox.pack_start(this.buttonTimer, !true, true);
			this.leftBox.pack_start(this.buttonSettings, !true, true);
			
		}
		protected Widget BuildAvatarButton()
		{
			string filename = tl_get_resource_path("images/avatar.png");
			var btn = new Button();
			btn.tooltip_text = "Mi Perfil";
			try
			{
				var pb = new Gdk.Pixbuf.from_file_at_scale(filename, 40, 40, true);
				
				btn.set_image(new Image.from_pixbuf( pb ));
				/*
				var menu = new GLib.Menu();
				var item1 = new GLib.MenuItem(this.cUser != null ? this.cUser.username : "Username", null);
				var item2 = new GLib.MenuItem("Cerrar Sesión", null);
				menu.append_item(item1);
				menu.append_item(item2);
				*/
				var popover = new Popover(btn);
				popover.position = Gtk.PositionType.RIGHT;
				popover.set_size_request(250, -1);
				popover.modal = true;
				//popover.bind_model(menu, null);
				
				var listbox = new ListBox();
				listbox.row_activated.connect( (row) => 
				{
					string key = row.get_data<string>("key");
					print("Menu key: %s\n", key);
					popover.visible = false;
					if( key == "profile" )
					{
						
					}
					else if( key == "switch_company" )
					{
						this.ShowCompanySelector();
					}
					else if( key == "close_session" )
					{
						this.CloseSession();
					}
				});
				var lbRow1 = new ListBoxRow();
				var lbRow2 = new ListBoxRow();
				this.listBoxRowSwitchCompany = new ListBoxRow();
				lbRow1.add(new Box(Orientation.VERTICAL, 5));
				this.listBoxRowSwitchCompany.add(new Box(Orientation.HORIZONTAL, 5));
				lbRow2.add(new Box(Orientation.HORIZONTAL, 5));
				
				lbRow1.set_data<string>("key", "profile");
				this.listBoxRowSwitchCompany.set_data<string>("key", "switch_company");
				lbRow2.set_data<string>("key", "close_session");
				
				(lbRow1.get_child() as Box).pack_start(
					this.labelUsername, 
					false, false
				);
				(this.listBoxRowSwitchCompany.get_child() as Box).pack_start(
					new Label("Cambiar Compañia"),
					false, false
				);
				(lbRow2.get_child() as Box).pack_start(
					new Label("Cerrar Sesión"), false, false
				);
				
				listbox.insert(lbRow1, -1);
				listbox.insert(this.listBoxRowSwitchCompany, -1);
				listbox.insert(lbRow2, -1);
				
				popover.add(listbox);
				popover.show_all();
				popover.visible = false;
				btn.clicked.connect( () => 
				{
					popover.visible = !popover.visible;
					//popover.popdown();
				});
				btn.show_all();
			}
			catch(GLib.Error e)
			{
				print("ERROR: %s\n", e.message);
			}
			return btn;
		}
		protected void BuildTopBar()
		{
			this.currentTask 			= new Label("");
			this.currentTask.xalign		= 0;
			this.currentProject			= new Label("");
			this.currentProject.xalign	= 0;
			this.widgetTimer			= new TimerLogs.Widgets.Timer();
			var box 					= new Box(Orientation.VERTICAL, 0);
			
			box.pack_start(this.currentTask, false, true);
			box.pack_start(this.currentProject, false, true);
			this.topBox.pack_start(box, false, true, 0);
			this.topBox.pack_start(this.widgetTimer, false, true, 0);
			//this.topBox.pack_start(new Label(""), true, true, 0);
			
			this.buttonStart = new Button();
			this.buttonStart.name = "button-start";
			this.buttonStart.set_halign(Align.END);
			this.buttonStart.set_valign(Align.START);
			this.buttonStart.set_image(new Image.from_file(tl_get_resource_path("images/play-24.png")));
			this.buttonStart.clicked.connect(this.OnButtonStartClicked);
			
			this.buttonPause = new Button();
			this.buttonPause.name = "button-pause";
			this.buttonPause.set_halign(Align.END);
			this.buttonPause.set_valign(Align.START);
			this.buttonPause.set_image(new Image.from_file(tl_get_resource_path("images/pause-24.png")));
			this.buttonPause.clicked.connect(this.OnButtonPauseClicked);
			
			this.overlay.add_overlay(this.buttonPause);
			this.overlay.add_overlay(this.buttonStart);
			
			this.buttonPause.visible = false;
		}
		protected void OnButtonStartClicked()
		{
			try
			{
				if( this.cProject == null )
					throw new TimerLogs.Exceptions.Project.INVALID_PROJECT("Proyecto invalido, debe seleccionar uno para poder iniciar el seguimiento.");
				if( this.cTask == null )
					throw new TimerLogs.Exceptions.Task.INVALID_TASK("Tarea invalida, debe seleccionar una tarea para poder iniciar el seguimiento.");
				
				this.widgetTimer.start();
				this.logger.Start(this.cProject, this.cTask, this.widgetTimer);
				this.buttonStart.visible = false;
				this.buttonPause.visible = true;
			}
			catch(TimerLogs.Exceptions.Project e)
			{
				var dlg = new MessageDialog(this, DialogFlags.MODAL, MessageType.ERROR, ButtonsType.CLOSE, e.message);
				dlg.title = "Error de Seguimiento";
				dlg.run();
				dlg.destroy();
			}
			catch(TimerLogs.Exceptions.Task e)
			{
				var dlg = new MessageDialog(this, DialogFlags.MODAL, MessageType.ERROR, ButtonsType.CLOSE, e.message);
				dlg.title = "Error de Seguimiento";
				dlg.run();
				dlg.destroy();
			}
		}
		protected void OnButtonPauseClicked()
		{
			this.widgetTimer.pause();
			this.logger.Stop();
			this.buttonPause.visible = false;
			this.buttonStart.visible = true;
		}
		protected void CloseSession()
		{
			var kfile = (KeyFile)SBGlobals.GetValue("kfile");
			kfile.set_string("Authentication", "jwt", "");
			save_config();
			this.contentBox.foreach( (c) => 
			{
				if( c != null )
					this.contentBox.remove(c);
				
			});
			this.currentCompany = null;
			this.companies.clear();
			this.widgetLogin.show();
			this.HideMain();
		}
		public void HideMain()
		{
			this.overlay.hide();
			//this.overlay.parent = this.auxBinContainer;
			//this.widgetLogin.parent = this;
		}
		public void ShowMain()
		{
			//this.HideLogin();
			//this.overlay.parent = this;
			this.overlay.show();
			this.resize(800, 600);
		}
		public void HideLogin()
		{
			this.widgetLogin.hide();
			//this.widgetLogin.parent = this.auxBinContainer;
		}
		public void ShowLogin()
		{
			this.widgetLogin.show();
			this.widgetLogin.reset();
			this.resize(800, 600);
		}
		protected void OnLoginSuccess(User user, string jwt)
		{
			this.cUser = user;
			this.ValidateSession();
		}
		public void ValidateSession()
		{
			this.HideLogin();
			var kfile 			= (KeyFile)SBGlobals.GetValue("kfile");
			try
			{
				string jwt			= kfile.get_string("Authentication", "jwt");
				int last_company_id	= kfile.get_integer("Settings", "last_company");
				//print("VALIDATION SESSION\n");
				//print("JWT: %s\nCompany id: %d\n", jwt, last_company_id);
				if( jwt.strip().length <= 0 )
				{
					this.title = "Iniciar Session - TimerLogs";
					this.ShowLogin();
					this.HideMain();
					return;
				}
			
				if( this.cUser == null )
				{
					var api 	= new ApiUsers();
					var res 	= api.profile(jwt);
					var dobj 	= res.getObjectData();
					this.cUser 	= new User();
					this.cUser.loadJsonObject(dobj);
					SBGlobals.SetValue("user", this.cUser);
				}
				print("Username: %s\n", this.cUser.username);
				this.title = "TimerLogs - Cliente del Trabajador";
				this.GetCompanies();
				print("Last company id: %d\n", last_company_id);
				if( last_company_id > 0 )
				{
					this.companies.foreach( (company) => 
					{
						if( company.id == last_company_id )
						{
							this.SetCompany(company);
							
							return false;
						}
						return true;
					});
				}
				
				if( this.currentCompany == null )
				{
					if( this.companies.size > 1 )
					{
						this.ShowCompanySelector();
					}
					else
					{
						//##use the first company
						this.currentCompany = this.companies.size == 1 ? this.companies.get(0) : null;
					}
				}
				if( this.currentCompany == null )
				{
					this.CloseSession();
					return;
				}
				//##save last company id
				kfile.set_integer("Settings", "last_company", (int)this.currentCompany.id);
				save_config();
				this.logger.SetUser(this.cUser);
				//this.labelUsername.label	= "%s %s\n%s".printf(this.cUser.first_name, this.cUser.last_name, this.currentCompany.name);
				//this.labelCompanyName.label	= this.currentCompany.name;
				this.ShowMain();
			}
			catch(GLib.KeyFileError e)
			{
				tl_log("KEY_FILE_ERROR: %s".printf(e.message));
			}
		}
		protected void GetCompanies()
		{
			tl_log("GETTINGS USER COMPANIES....		");
			var api 	= ApiTimerLogs.getInstance(); //new ApiTimerLogs();
			var res 	= api.getCompanies();
			var data 	= res.getArrayData();
			this.companies.clear();
			foreach(var node in data.get_elements())
			{
				var c = new Company();
				c.loadJsonObject(node.get_object());
				this.companies.add(c);
				tl_log(@"Company: $(c.name)\n");
			}
			tl_log("[DONE]\n");
		}
		protected void SetProjects()
		{
			this.contentBox.foreach( (c) => 
			{
				if( c != null )
					this.contentBox.remove(c);
				
			});
			tl_log(@"Setting projects for company \"$(this.currentCompany.name)\"...\n");
			var plist = new ProjectsTasks(this.currentCompany);
			this.contentBox.pack_start(plist, true, true, 0);
			plist.project_selected.connect( this.OnProjectSelected );
			plist.task_selected.connect( this.OnTaskSelected );
			plist.show_all();
		}
		protected void OnProjectSelected(TimerLogs.Models.Project? project)
		{
			if( project == null )
				return;
				
			this.cProject 				= project;
			this.cTask 					= null;
			this.currentTask.label 		= "";
			this.currentProject.label 	= project.name;
		}
		protected void OnTaskSelected(TimerLogs.Models.Task? task)
		{
			
			this.cTask = task;
			this.currentTask.label = task.name;
			this.widgetTimer.setWorkedTimeData( new TimeData.fromTaskData(task.tdata) );
		}
		protected void ShowCompanySelector()
		{
			print("Show company selector\n");
			var dlg = new TimerLogs.Dialogs.DialogSelectCompany();
			dlg.SetCompanies(this.companies);
			int res = dlg.run();
			print("Dialog result: %d\n", res);
			dlg.close();
			dlg.destroy();
			if( res == 1 )
			{
				this.SetCompany(dlg.GetCompanySelected());
			}
		}
		protected void SetCompany(Company c)
		{
			this.currentCompany = c;
			this.labelUsername.set_markup(
				"<b>%s %s</b>\n<span style=\"italic\">%s</span>"
				.printf(this.cUser.first_name, this.cUser.last_name, this.currentCompany.name)
			);
			this.SetProjects();
			print("Company: %s\n", this.currentCompany.name);
		}
		protected void OnButtonSettingsClicked()
		{
			var widgetUserProfile = new UserProfile();
			widgetUserProfile.show_all();
			widgetUserProfile.setUser(this.cUser);
			
			var dialog 		= new Dialog.with_buttons("Configuración", this, DialogFlags.MODAL);
			var btnClose 	= dialog.add_button("Cerrar", ResponseType.NONE);
			var btnSave		= dialog.add_button("Guardar", ResponseType.APPLY);
			
			btnClose.get_style_context().add_class("btn");
			btnClose.get_style_context().add_class("btn-danger");
			btnSave.get_style_context().add_class("btn");
			btnSave.get_style_context().add_class("btn-primary");
			
			dialog.get_content_area().pack_start(widgetUserProfile, true, true);
			dialog.get_content_area().set_spacing(10);
			dialog.set_size_request(400, 400);
			int res = dialog.run();
			dialog.destroy();
			if( res == ResponseType.APPLY )
			{
				widgetUserProfile.save();
			}
		}
		/**
		 * Send last data to server, so we need to set the disconnect data and online state
		 */
		public void ShutdownBeforeClose()
		{
			tl_log("Sending disconnect data and state to server");
		}
	}
}
