#include <stdio.h>
#include <time.h>
#include <windows.h>
#include <uiautomation.h>
//#include <png.h>
#include <psapi.h>

typedef struct _WinApplicationData
{
	char	applicationName[MAX_PATH];
	char	processName[MAX_PATH];
	char	moduleName[MAX_PATH];
	char	operatingSystem[128];
}WinApplicationData;
