/*
#define WINVER 0x0501
#define _WIN32_WINNT 0x0501
#include <Windows.h>
#include <UIAutomation.h>
*/
#include <stdio.h>
#include <time.h>
#include <windows.h>
#include <uiautomation.h>
#include <psapi.h>
#include <gdk/gdkwin32.h>
//#include "capture.h"
#include <KeyboardMouseData.h>

int CaptureAnImage(void* hWnd, char* filename)
{
	HDC	hdcScreen;
	HDC hdcWindow;
	HDC hdcMemDC 		= NULL;
	HBITMAP hbmScreen 	= NULL;
	BITMAP bmpScreen;
	
	hdcScreen 	= GetDC(NULL);
	hdcWindow	= GetDC(hWnd);
	hdcMemDC	= CreateCompatibleDC(hdcWindow);
	if( !hdcMemDC )
	{
		MessageBox(hWnd, "CreateCompatibleDC has failed", "Failed", MB_OK);
		goto done;
	}
	int cxScreen = GetSystemMetrics(SM_CXSCREEN);
	int cyScreen = GetSystemMetrics(SM_CYSCREEN);
	
	RECT rcClient;
	GetClientRect(hWnd, &rcClient);
	SetStretchBltMode(hdcWindow, HALFTONE);
	if( !StretchBlt(hdcWindow, 0, 0, rcClient.right, rcClient.bottom, hdcScreen, 0, 0, cxScreen, cyScreen, SRCCOPY | CAPTUREBLT ) )
	{
		MessageBox(hWnd, "StretchBlt has failed", "Failed", MB_OK);
		goto done;
	}
	//##create compatible Bitmap
	hbmScreen = CreateCompatibleBitmap(hdcWindow, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
	if( !hbmScreen )
	{
		MessageBox(hWnd, "CreateCompatibleBitmap failed", "Failed", MB_OK);
		goto done;
	}
	// select the compatible bitmap into compatible memory DC
	SelectObject(hdcMemDC, hbmScreen);
	// Bit block transfer into our compatible memory DC
	if( !BitBlt(hdcMemDC, 0, 0, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, hdcWindow, 0, 0, SRCCOPY | CAPTUREBLT) )
	{
		MessageBox(hWnd, "BitBlt has failed", "Failed", MB_OK);
		goto done;
	}
	GetObject(hbmScreen, sizeof(BITMAP), &bmpScreen);
	BITMAPFILEHEADER bmfHeader;
	BITMAPINFOHEADER bi;
	
	bi.biSize 			= sizeof(BITMAPINFOHEADER);
	bi.biWidth			= bmpScreen.bmWidth;
	bi.biHeight			= bmpScreen.bmHeight;
	bi.biPlanes			= 1;
	bi.biBitCount		= 32;
	bi.biCompression	= BI_RGB;
	bi.biSizeImage		= 0;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;
	
	DWORD dwBmpSize = ((bmpScreen.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;
	HANDLE hDIB		= GlobalAlloc(GHND, dwBmpSize);
	char *lpbitmap	= (char *)GlobalLock(hDIB);
	
	GetDIBits(hdcWindow, hbmScreen, 0, (UINT)bmpScreen.bmHeight, lpbitmap, (BITMAPINFO *)&bi, DIB_RGB_COLORS);
	
	HANDLE hFile 		= CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dwSizeofDIB 	= dwBmpSize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	bmfHeader.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof(BITMAPINFOHEADER);
	bmfHeader.bfSize	= dwSizeofDIB;
	bmfHeader.bfType	= 0x4D42; //BM
	
	DWORD dwBytesWritten = 0;
	WriteFile(hFile, (LPSTR)&bmfHeader, sizeof(BITMAPFILEHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)&bi, sizeof(BITMAPINFOHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)lpbitmap, dwBmpSize, &dwBytesWritten, NULL);
	
	//Unlock and Free the DIB from the heap
	GlobalUnlock(hDIB);
	GlobalFree(hDIB);
	
	CloseHandle(hFile);

done:
	DeleteObject(hbmScreen);
	DeleteObject(hdcMemDC);
	ReleaseDC(NULL, hdcScreen);
	ReleaseDC(hWnd, hdcWindow);
	printf("Desktop has been captured\n");
	return 0;
}
void* getActiveWindowData()
{
	//WinApplicationData* appData = (WinApplicationData*)malloc(sizeof(WinApplicationData));
	HookApplicationData* appData = malloc(sizeof(HookApplicationData));
	
	//strcpy(appData->operatingSystem, "Windows");
	
	HWND	hWnd = GetForegroundWindow();
	//GetTitleBarInfo(hWnd, windowData);
	DWORD pid;
	DWORD res 	= GetWindowThreadProcessId(hWnd, &pid);
	HANDLE ph	= OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);
	TCHAR nameBuffer[MAX_PATH];
	//TCHAR szProcessName[MAX_PATH];
	//GetModuleFileName(ph, nameBuffer, MAX_PATH);
	
	DWORD ns;
	QueryFullProcessImageName(ph, 0, nameBuffer, &ns);
	//strcpy(appData->processName, nameBuffer);
	strcpy(appData->process, nameBuffer);
	
	HMODULE hMod;
	DWORD cbNeeded;
	if( EnumProcessModules(ph, &hMod, sizeof(hMod), &cbNeeded) )
	{
		//GetModuleBaseName(ph, hMod, appData->moduleName, sizeof(appData->moduleName) / sizeof(TCHAR));
		GetModuleBaseName(ph, hMod, appData->filename, sizeof(appData->filename) / sizeof(TCHAR));
	}
	CloseHandle(ph);
	int len = GetWindowTextLength(hWnd);
	char* windowTitle = (char*)calloc(1, len + 1);
	GetWindowText(hWnd, windowTitle, len + 1);
	//strcpy(appData->applicationName, windowTitle);
	strcpy(appData->name, windowTitle);
	free(windowTitle);
	/*
	printf("Process Name: %s\n", appData->processName);
	printf("Module Name: %s\n", appData->moduleName);
	printf("Current Window: %s\n", appData->applicationName);
	*/
	
	/*
	HRESULT hres;
	IUIAutomationElement *aElement;
	
	hres = ElementFromHandle(hWnd, aElement);
	*/
	return (void*)appData;
}
HINSTANCE getApplicationInstance()
{
	return GetModuleHandle(NULL);
}
static LRESULT CALLBACK message_window_handler(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) 
{
	printf("\n====================\nWINDOW MESSAGE: %u\n\n", msg);
	return TRUE;
}
void setMessageWindowHandler(void* gdkWindow)
{
	
	HINSTANCE hInst = gdk_win32_window_get_handle(GDK_WINDOW(gdkWindow));
	HWND win_hwnd;
	WNDCLASSEX wcx;
	LPCTSTR wname;
	wname = TEXT("WinpidginMsgWinCls");
	
	wcx.cbSize = sizeof(wcx);
	wcx.style = 0;
	wcx.lpfnWndProc = message_window_handler;
	wcx.cbClsExtra = 0;
	wcx.cbWndExtra = 0;
	wcx.hInstance = hInst;
	wcx.hIcon = NULL;
	wcx.hCursor = NULL;
	wcx.hbrBackground = NULL;
	wcx.lpszMenuName = NULL;
	wcx.lpszClassName = wname;
	wcx.hIconSm = NULL;
	
	ATOM res = RegisterClassEx(&wcx);
	printf("MESSAGE HANDLER ADDED: %u\n", res);
}
#ifdef __TEST__
int main(int argc, char** argv)
{
	HWND hDesktopWnd = GetDesktopWindow();
	CaptureImage(hDesktopWnd);
	return 0;
}
#endif
