#include "win-hooks.h"

char const ascii_table[] = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ Çüéâ";

HINSTANCE				hinst;
HHOOK 					hhk;
HHOOK					mouseHook;
HookKeyboardMouseData 	*currentData;
int 					applicationCount = 0;
int						INACTIVITY_TIME = 60;

time_t	mouseTime, keyboardTime, globalTime;

void set_inactivity_time(int seconds)
{
	INACTIVITY_TIME = seconds;
}
int char_to_ascii(int ch)
{
    char const *p = strchr(ascii_table, ch);
    return p ? p - ascii_table + 32 : 0;
}

int ascii_to_char(int a)
{
     char ascii = (a >= 32 && a < 128) ? ascii_table[a - 32] : 0;
     
     return ascii;
}
HookKeyboardMouseData* HookKeyboardMouseData_new()
{
	HookKeyboardMouseData* obj	= (HookKeyboardMouseData*)malloc(sizeof(HookKeyboardMouseData));
	obj->clicks 			= 0;
	obj->leftClicks 		= 0;
	obj->rightClicks 		= 0;
	obj->keydowns			= 0;
	obj->keyboardInactivity	= 0;
	obj->mouseInactivity	= 0;
	obj->inactivity			= 0;
	obj->applications		= NULL;
	
	return obj;
}
void HookKeyboardMouseData_free(HookKeyboardMouseData* obj)
{
	free(obj);
}

HookApplicationData* HookApplicationData_new()
{
	HookApplicationData* obj = malloc(sizeof(HookApplicationData));
	obj->timestamp = 0;
	return obj;
}
void HookApplicationData_free(HookKeyboardMouseData* obj)
{
	free(obj);
}

HookApplicationData* hookGetActiveWindowData()
{
	HookApplicationData *appData = NULL;
	
	HWND	hWnd = GetForegroundWindow();
	if( hWnd == NULL )
	{
		printf("ERROR: Unable to get foreground window\n");
		return NULL;
	}
	appData = HookApplicationData_new();
	appData->timestamp = time(NULL) * 1;
	//printf("appData->timestamp: %lu\n", appData->timestamp);
	
	DWORD pid;
	DWORD res 	= GetWindowThreadProcessId(hWnd, &pid);
	//printf("PID: %lu\n", pid);
	HANDLE ph	= OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);
	if( ph == NULL )
	{
		DWORD errorCode = GetLastError();
		printf("ERROR: unable to open process, CODE: %lu\n", errorCode);
		free(appData);
		return NULL;
	}
	TCHAR nameBuffer[1024];
	
	DWORD ns;
	BOOL r = QueryFullProcessImageName(ph, 0, nameBuffer, &ns);
	//printf("RES: %d\n", r);
	if( r != 0 )
	{
		//size_t n;
		//memcpy(appData->process, nameBuffer, &n);
		strcpy(appData->process, nameBuffer);
		printf("appData->process: %s\nSize: %lu\n", appData->process, ns);
	}
	else
	{
		strcpy(appData->process, "Unknown");
		DWORD errorCode = GetLastError();
		printf("ERROR: unable to get process name, CODE: %lu\n", errorCode);
	}
	//printf("appData->process: %s\n", appData->process);
	HMODULE hMod;
	DWORD cbNeeded;
	if( EnumProcessModules(ph, &hMod, sizeof(hMod), &cbNeeded) )
	{
		GetModuleBaseName(ph, hMod, appData->filename, sizeof(appData->filename) / sizeof(TCHAR));
		//GetModuleBaseName(ph, hMod, appData->filename, sizeof(appData->filename) / sizeof(char*));
		//printf("appData->filename: %s\n", appData->filename);
	}
	CloseHandle(ph);
	int len = GetWindowTextLength(hWnd);
	char* windowTitle = (char*)calloc(1, len + 1);
	GetWindowText(hWnd, windowTitle, len + 1);
	strcpy(appData->name, windowTitle);
	//printf("appData->name: %s -> %s\n", appData->name, windowTitle);
	free(windowTitle);
	return appData;
}
double getKeyboardInactivity()
{
	size_t endTime 	= time(NULL);
	double timeDiff = difftime(endTime, keyboardTime);
	keyboardTime 	= time(NULL);
	return timeDiff > INACTIVITY_TIME ? timeDiff : 0;
}
double getMouseInactivity()
{
	size_t endTime 	= time(NULL);
	double timeDiff = difftime(endTime, mouseTime);
	mouseTime 		= time(NULL);
	return timeDiff > INACTIVITY_TIME ? timeDiff : 0;
}
double getGlobalInactivity()
{
	size_t endTime	= time(NULL);
	double timeDiff	= difftime(endTime, globalTime);
	globalTime		= time(NULL);
	
	return timeDiff > INACTIVITY_TIME ? timeDiff : 0;
}
LRESULT wireKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if( wParam == WM_KEYUP )
	{
		//KBDLLHOOKSTRUCT* data = (KBDLLHOOKSTRUCT*)lParam;
		//DWORD _key = data->vkCode;
		//fprintf(stdout, "KEYWORD HOOK: KEYDOWN -> %lu:%c\n", data->vkCode, ascii_to_char(data->vkCode));
		//fflush(stdout);
		currentData->keydowns++;
		currentData->keyboardInactivity += getKeyboardInactivity();
		keyboardTime = time(NULL);
	}
	currentData->inactivity += getGlobalInactivity();
	LRESULT retVal = CallNextHookEx(hhk, nCode, wParam, lParam);
	return retVal;
}
LRESULT wireMouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if( wParam == WM_LBUTTONUP )
	{
		currentData->leftClicks++;
		currentData->inactivity += getGlobalInactivity();
		//fprintf(stdout, "MOUSE HOOK: LEFT BUTTON CLICK\n");
		//fflush(stdout);
		HookApplicationData *data = hookGetActiveWindowData();
		
		if( data != NULL )
		{
			if( applicationCount == 0 )
			{
				printf("application stack is NULL, initializing\n");
				currentData->applications 	= (HookApplicationData**)malloc(sizeof(HookApplicationData *) * 2);
			}
			else
			{
				int newLength = applicationCount + 1;
				currentData->applications = realloc(currentData->applications, sizeof(HookApplicationData*) * (newLength + 1));
				if( currentData->applications == NULL )
				{
					printf("ERROR: Error while trying to reasign memory for applications stack\n");
				}
			}
			currentData->applications[applicationCount] = data;
			currentData->applications[applicationCount + 1] = NULL;
			/*
			printf("CAPTURED WINDOWS DATA\n\nName: %s Filename: %s  Process: %s Timestamp: %lu\n",
				currentData->applications[applicationCount]->name,
				currentData->applications[applicationCount]->filename,
				currentData->applications[applicationCount]->process,
				currentData->applications[applicationCount]->timestamp
			);
			*/
			applicationCount++;
			
			
			fflush(stdout);
			//## ger mouse inactivity
			currentData->mouseInactivity += getMouseInactivity();
		}
		
	}
	else if( wParam == WM_RBUTTONUP )
	{
		currentData->rightClicks++;
		//## ger mouse inactivity
		currentData->mouseInactivity 	+= getMouseInactivity();
		currentData->inactivity 		+= getGlobalInactivity();
	}
	currentData->clicks = currentData->leftClicks + currentData->rightClicks;
		
	LRESULT retVal = CallNextHookEx(hhk, nCode, wParam, lParam);
	return retVal;
}
void init()
{
	currentData			= HookKeyboardMouseData_new();
	mouseTime 			= time(NULL);
	keyboardTime 		= time(NULL);
	globalTime			= time(NULL);
	applicationCount 	= 0;
}
HookKeyboardMouseData* getData()
{
	if( currentData->keydowns <= 0 || currentData->clicks <= 0 )
		currentData->inactivity += getGlobalInactivity();
	if( currentData->keydowns <= 0 )
    {
		currentData->keyboardInactivity += getKeyboardInactivity();
	}
	if( currentData->clicks <= 0 )
	{
		currentData->mouseInactivity += getMouseInactivity();
	}
	HookKeyboardMouseData *data = currentData;
	//##reset data
	init();
	return (void*)data;
}
void install()
{
	//fprintf(stdout, "INSTALLING KEYBOARD HOOKS\n");
	//fflush(stdout);
	hhk = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)wireKeyboardProc, hinst, 0);
	//fprintf(stdout, "INSTALLING MOUSE HOOKS\n");
	//fflush(stdout);
	mouseHook = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)wireMouseProc, hinst, 0);
	init();
}
void uninstall()
{
	if( hhk != NULL)
	{
		fprintf(stdout, "UNINSTALL KEYBOARD HOOKS\n");
		fflush(stdout);
		UnhookWindowsHookEx(hhk); 
	}
	if( mouseHook != NULL )
	{
		fprintf(stdout, "UNINSTALL MOUSE HOOKS\n");
		fflush(stdout);
		UnhookWindowsHookEx(mouseHook); 
	}
	
}
BOOL WINAPI DllMain(__in HINSTANCE hinstDLL, __in DWORD fdwReason, __in LPVOID lpvReserved)
{
	hinst = hinstDLL;
	return TRUE;
}
