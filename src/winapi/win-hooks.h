//#define _WIN32_WINNT 0x0501
#include <windows.h>
#include <psapi.h>
#include <stdio.h>
#include <time.h>

typedef struct hookApplicationData
{
	char			name[MAX_PATH];
	char			filename[MAX_PATH];
	char			process[1024];
	unsigned long	timestamp;
}HookApplicationData;

typedef struct hookKeyboardMouseData
{
	unsigned long 		clicks;
	unsigned long		leftClicks;
	unsigned long		rightClicks;
	unsigned long		keydowns;
	double				keyboardInactivity;
	double				mouseInactivity;
	double				inactivity;
	HookApplicationData	**applications;
}HookKeyboardMouseData;

HookKeyboardMouseData* HookKeyboardMouseData_new();
void HookKeyboardMouseData_free(HookKeyboardMouseData* obj);

HookApplicationData* HookApplicationData_new();
void HookApplicationData_free(HookKeyboardMouseData* obj);

double getMouseInactivity();
