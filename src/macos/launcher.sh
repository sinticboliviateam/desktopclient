#!/bin/bash

if [ ! -d $HOME/.config ]; then
	mkdir $HOME/.config
fi

if [ ! -d $HOME/.config ]; then
	mkdir $HOME/.config/tl
fi

cd "${0%/*}"

CURRENT_DIR=`pwd`
GTK_HOME="${CURRENT_DIR}/../Frameworks/gtk3"
bundle_dir="${CURRENT_DIR}/../"

export DYLD_LIBRARY_PATH="${GTK_HOME}/lib"
#export XDG_CONFIG_DIRS="${GTK_HOME}/etc"/xdg
#export XDG_DATA_DIRS="${GTK_HOME}/share"
#export GTK_DATA_PREFIX="${GTK_HOME}/share"
#export GTK_EXE_PREFIX="${GTK_HOME}/bin"
export GTK_PATH="${GTK_HOME}"
export GIO_MODULE_DIR="${GTK_HOME}/lib/gio/modules"

# PANGO_* is no longer needed for pango >= 1.38
#export PANGO_RC_FILE="$bundle_etc/pango/pangorc"
#export PANGO_SYSCONFDIR="$bundle_etc"
#export PANGO_LIBDIR="$bundle_lib"

# Pixbuf plugins
export GDK_PIXBUF_MODULEDIR="${GTK_HOME}/lib/gdk-pixbuf-2.0/2.10.0/loaders"
export GDK_PIXBUF_MODULE_FILE="${GTK_HOME}/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache"

./timerlogs
