//
//  HookApplicationData.h
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//

#ifndef HookApplicationData_h
#define HookApplicationData_h
typedef struct hookApplicationData
{
    char            name[512];
    char            filename[512];
    char            process[1024];
    unsigned long   timestamp;
}HookApplicationData;

HookApplicationData* HookApplicationData_new(void);
void HookApplicationData_free(HookApplicationData* obj);

#endif /* HookApplicationData_h */
