//
//  HookApplicationData.m
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HookApplicationData.h"

void tl_log(char* dump_text);

HookApplicationData* HookApplicationData_new()
{
    HookApplicationData* obj = malloc(sizeof(HookApplicationData));
    obj->timestamp = 0;
    return obj;
}
void HookApplicationData_free(HookApplicationData* obj)
{
    if( obj == NULL )
        return;
    tl_log("HookApplicationData_free...    ");
    free(obj);
    tl_log("[DONE]");
}
