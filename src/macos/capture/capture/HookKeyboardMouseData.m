//
//  HookKeyboardMouseData.m
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HookKeyboardMouseData.h"
void tl_log(char *dump_text);

HookKeyboardMouseData* HookKeyboardMouseData_new()
{
    HookKeyboardMouseData* obj  = malloc(sizeof(HookKeyboardMouseData));
    obj->clicks                 = 0;
    obj->leftClicks             = 0;
    obj->rightClicks            = 0;
    obj->keydowns               = 0;
    obj->keyboardInactivity     = 0;
    obj->mouseInactivity        = 0;
    obj->inactivity				= 0;
    obj->applications           = NULL;
    
    return obj;
}
void HookKeyboardMouseData_free(HookKeyboardMouseData* obj)
{
    if( obj == NULL )
        return;
    tl_log("HookKeyboardMouseData_free");
    if( obj->applications != NULL )
    {
        for(int i = 0;;i++)
        {
            //tl_log("");
            if( obj->applications[i] == NULL )
                break;
            else
                HookApplicationData_free( obj->applications[i] );
        }
        tl_log("END applications loop");
    }
    tl_log("HookKeyboardMouseData_free: [DONE]");
    if( obj != NULL )
        free(obj);
}
