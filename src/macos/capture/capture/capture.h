//
//  capture.h
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//

#import <Foundation/Foundation.h>

void capture_desktop(char* filename);
void fast_capture_desktop(char* destination);
bool check_accesibility_access(void);
bool check_capture_screen_access(void);
