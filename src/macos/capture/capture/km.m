//
//  km.m
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import "km.h"
#import <string.h>

//event tap
static CFMachPortRef eventTap = NULL;
//event mask
CGEventMask eventMask = 0;
//run loop source
CFRunLoopSourceRef runLoopSource = NULL;
HookKeyboardMouseData *currentData = NULL;
time_t mouseTime, keyboardTime, globalTime;
int     applicationCount = 0;
bool    eventsStarted = false;
char    log_filename[PATH_MAX] = "timerlogs.log";
int     INACTIVITY_TIME = 30;

void tl_set_log_file(char* filename)
{
    strcpy(log_filename, filename);
}
void tl_log(char* dump_text)
{
    char str[2048];
    sprintf(str, "[LIBCAPTURE]: %s\n", dump_text);
    /*
    strcat(str, "[LIBCAPTURE]: ");
    strcat(str, dump_text);
    strcat(str, "\n");
    */
    /*
    FILE *fh = fopen(log_filename, "r");
    if( fh == NULL )
    {
        fh = fopen(log_filename, "w+");
    }
    else
        fh = fopen(log_filename, "a+");
    fputs(str, fh);
    fclose(fh);
    */
    printf("%s\n", str);
}
void init()
{
    currentData         = HookKeyboardMouseData_new();
    mouseTime           = time(NULL);
    keyboardTime        = time(NULL);
    globalTime          = time(NULL);
    applicationCount    = 0;
}
double getMouseInactivity()
{
    size_t endTime  = time(NULL);
    double timeDiff = difftime(endTime, mouseTime);
    mouseTime       = time(NULL);
    return (timeDiff >= INACTIVITY_TIME) ? timeDiff : 0;
}
double getKeyboardInactivity()
{
    size_t endTime  = time(NULL);
    double timeDiff = difftime(endTime, keyboardTime);
    keyboardTime    = time(NULL);
    return ( timeDiff >= INACTIVITY_TIME ) ? timeDiff : 0;
}
double getGlobalInactivity()
{
	size_t endTime	= time(NULL);
	double timeDiff	= difftime(endTime, globalTime);
	globalTime		= time(NULL);
	
	return timeDiff >= INACTIVITY_TIME ? timeDiff : 0;
}
CGEventRef eventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon)
{
    if( currentData == NULL )
    {
        tl_log("Current data is NULL");
        return event;
    }
    
    if( type == kCGEventLeftMouseUp )
    {
        currentData->mouseInactivity += getMouseInactivity();
        currentData->clicks++;
        currentData->leftClicks++;
        currentData->inactivity += getGlobalInactivity();
        HookApplicationData *appData = getActiveWindowData();
        if( applicationCount == 0 )
        {
            currentData->applications = (HookApplicationData**)malloc(sizeof(HookApplicationData*) * 2);
        }
        else
        {
            int newLength = applicationCount + 1;
            currentData->applications = realloc(currentData->applications, sizeof(HookApplicationData*) * (newLength + 1));
            if( currentData->applications == NULL )
            {
                tl_log("ERROR: Error while trying to reasign memory for applications stask\n");
            }
        }
        currentData->applications[applicationCount] = appData;
        currentData->applications[applicationCount + 1] = NULL;
        applicationCount++;
    }
    else if( type == kCGEventRightMouseUp )
    {
        currentData->mouseInactivity += getMouseInactivity();
        currentData->clicks++;
        currentData->rightClicks++;
        currentData->inactivity += getGlobalInactivity();
        //printf("Mouse right click\n");
    }
    else if( type == kCGEventKeyDown )
    {
        //printf("Keyboard keydown\n");
        currentData->keydowns++;
        currentData->keyboardInactivity += getKeyboardInactivity();
        currentData->inactivity += getGlobalInactivity();
    }
    else if( type == kCGEventTapDisabledByTimeout )
    {
        CGEventTapEnable(eventTap, true);
        tl_log("Event tap timed out: restarting tap");
        return event;
    }
    return event;
}
void observer_callback(CFRunLoopObserverRef observer, CFRunLoopActivity activity, void *info)
{
    printf("observer_callback\n");
}
void start_km_capture()
{
    if( currentData != NULL )
        HookKeyboardMouseData_free(currentData);
    init();
    if( eventsStarted )
    {
        if( eventTap != NULL )
            CGEventTapEnable(eventTap, true);
        tl_log("The keyboard and mouse events are already started");
        return;
    }
    tl_log("Starting keyboard and mouse events");
    
    @autoreleasepool
    {
        /*
        if( 0 != geteuid() )
        {
            //err msg/bail
            printf("ERROR: run as root\n\n");
            _clean_km_capture();
            return;
        }
        */
        eventMask = CGEventMaskBit(kCGEventLeftMouseDown) | CGEventMaskBit(kCGEventLeftMouseUp)| CGEventMaskBit(kCGEventRightMouseDown) | CGEventMaskBit(kCGEventRightMouseUp) |
            CGEventMaskBit(kCGEventKeyDown) | CGEventMaskBit(kCGEventKeyUp);
        //create event tap
        eventTap = CGEventTapCreate(kCGSessionEventTap, kCGHeadInsertEventTap, 0, eventMask, eventCallback, NULL);
        if(NULL == eventTap)
        {
            //err msg/bail
            tl_log("ERROR: failed to create event tap");
            _clean_km_capture();
            return;
        }
        //dbg msg
        tl_log("created event tap");
        
        //run loop source
        runLoopSource = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, eventTap, 0);
        //add to current run loop.
        //CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopCommonModes);
        CFRunLoopAddSource(CFRunLoopGetMain(), runLoopSource, kCFRunLoopCommonModes);
        //enable tap
        CGEventTapEnable(eventTap, true);
        eventsStarted = true;
        //dbg msg
        tl_log("enabled event tap to commence sniffing\n\n");
        //start loop for events only for dynamically loaded libraries or programs with its own loop
        //CFRunLoopRun();
        /*
        CFRunLoopObserverRef observer;
        observer = CFRunLoopObserverCreate(NULL, kCFRunLoopAllActivities, true, 0, observer_callback, NULL);
        CFRunLoopAddObserver(CFRunLoopGetMain(), observer, kCFRunLoopCommonModes);
        */
    }
}
void _clean_km_capture()
{
    //release event tap
    if(NULL != eventTap)
    {
        CFRelease(eventTap);
        eventTap = NULL;
    }
    //release run loop src
    if(NULL != runLoopSource)
    {
        //CFRunLoopRemoveSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopCommonModes);
        CFRelease(runLoopSource);
        runLoopSource = NULL;
    }
    if( currentData != NULL )
        HookKeyboardMouseData_free(currentData);
    currentData = NULL;
}
void stop_km_capture()
{
    tl_log("stop_km_capture");
    //CFRunLoopStop(CFRunLoopGetCurrent());
    //_clean_km_capture();
    //CFRunLoopStop(CFRunLoopGetCurrent());
    if( eventTap != NULL )
         CGEventTapEnable(eventTap, false);
    if( currentData != NULL )
           HookKeyboardMouseData_free(currentData);
    currentData = NULL;
    tl_log("Run loop stoped");
}
/**
 * Get the current application data
 * *
 */
HookApplicationData* getActiveWindowData()
{
    //CGWindowListOption(kCGWindowListOptionAll);
    HookApplicationData *appData = HookApplicationData_new();
    @autoreleasepool {
        CFArrayRef windowList = CGWindowListCopyWindowInfo(
                                                           //kCGWindowListOptionAllOnScreenAboveWindow
                                                           //kCGWindowListOptionAll,
                                                           kCGWindowListOptionOnScreenOnly,
                                                           kCGNullWindowID
        );
        long totalWindows = CFArrayGetCount(windowList);
        int topmostId = [[[NSWorkspace sharedWorkspace] frontmostApplication] processIdentifier];
        
        for(int i = 0; i < totalWindows; i++)
        {
            CFDictionaryRef window      = CFArrayGetValueAtIndex(windowList, i);
            CFNumberRef ownerPidObj     = (CFNumberRef)CFDictionaryGetValue(window, kCGWindowOwnerPID);
            CFStringRef windowNameObj   = (CFStringRef)CFDictionaryGetValue(window, kCGWindowName);
            CFNumberRef windowNumObj    = (CFNumberRef)CFDictionaryGetValue(window, kCGWindowNumber);
            CFStringRef ownerNameObj    = (CFStringRef)CFDictionaryGetValue(window, kCGWindowOwnerName);
            
            if( windowNameObj == NULL )
                continue;
            int ownerPid = 0;
            int windowNum = 0;
                   
            CFNumberGetValue(ownerPidObj, kCFNumberIntType, &ownerPid);
            CFNumberGetValue(windowNumObj, kCFNumberIntType, &windowNum);
            if( ownerPid != topmostId )
                continue;
                  
            //char ownerName[size + 1];
            //CFStringEncoding encodingMethod = CFStringGetSystemEncoding();
            //CFStringGetCString(ownerNameObj, ownerName, size, encodingMethod);
            //const char *ownerName = CFStringGetCStringPtr(ownerNameObj, kCFStringEncodingMacRomanLatin1);
            const char *ownerName   = [(__bridge NSString*)ownerNameObj UTF8String];
            const char *windowName  = [(__bridge NSString*)windowNameObj UTF8String];
            
            if( windowName == NULL || strlen(windowName) <= 0 )
                continue;
            strcpy(appData->filename, "");
            strcpy(appData->name, windowName);
            strcpy(appData->process, ownerName);
            appData->timestamp = time(NULL) * 1;
            
            //NSLog(@"Front top most id: %d\n", topmostId);
            //NSLog(@"Window index: %d\n", i);
            //NSLog(@"%@", CFDictionaryGetValue(window, kCGWindowBounds));
            NSLog(@"Window Name: %s, Owner id: %d, Owner Name: %s\n", windowName, ownerPid, ownerName);
            //NSLog(@"WindowNumber: %d\n", windowNum);
            //NSLog(@"Owner Name Dump: %@\n", ownerNameObj);
            NSLog(@"====================================================\n\n\n");
            //##just capture the first application match, so we need to break the loop
            break;
        }
    }
    
    
    return appData;
}
HookKeyboardMouseData* getData()
{
    if( currentData == NULL )
        return NULL;
    
    if( currentData->keydowns <= 0 || currentData->clicks <= 0 )
        currentData->inactivity += getGlobalInactivity();
    if( currentData->keydowns <= 0 )
    {
        currentData->keyboardInactivity += getKeyboardInactivity();
    }
    if( currentData->clicks <= 0 )
    {
        currentData->mouseInactivity += getMouseInactivity();
    }
    HookKeyboardMouseData *data = currentData;
    
    //##reset data
    init();
    return data;
}
