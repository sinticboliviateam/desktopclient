//
//  capture.m
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//
#include <ApplicationServices/ApplicationServices.h>
#include <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import "capture.h"
#import <CoreGraphics/CoreGraphics.h>
#import <string.h>

void tl_log(char* dump_text);

void capture_desktop(char* filename)
{
    NSString* _filename = [NSString stringWithUTF8String:filename];
    NSURL* url = [NSURL fileURLWithPath: _filename];

    /*
    CFURLRef url = CFURLCreateWithString(kCFAllocatorDefault, (__bridge CFStringRef)filename, NULL);
    */
    /*
     CGDisplayStreamRef stream = CGDisplayStreamCreate(CGMainDisplayID(), 1, 1, kCVPixelFormatType_32BGRA, nil,
     ^(CGDisplayStreamFrameStatus status, uint64_t displayTime, IOSurfaceRef frameSurface, CGDisplayStreamUpdateRef updateRef) {});
     */
    
    CGImageRef img = CGDisplayCreateImage(CGMainDisplayID());
    CGImageDestinationRef dest = CGImageDestinationCreateWithURL((CFURLRef)url, kUTTypeJPEG, 1, nil);
    CGImageDestinationAddImage(dest, img, nil);
    CGImageDestinationFinalize(dest);
    CGImageRelease(img);
    
    char log[1024];
    strcat(log, "CAPTURE DESKTOP: [DONE]\n");
    strcat(log, "DESTINATION: ");
    strcat(log, filename);
    tl_log(log);
    /*
    Capture* instance = (__bridge Capture*)obj;
    NSString* nsFilename = [[NSString alloc] initWithFormat: @"%s", filename];// [NSString stringWithUTF8String:filename];
    tl_log((char*)nsFilename.UTF8String);
    [instance captureDesktop:nsFilename];
    */
}
void fast_capture_desktop(char* destination)
{
    //void* obj = capture_new();
    capture_desktop(destination);
    //capture_free(obj);
}
bool check_capture_screen_access()
{
    bool pass = false;
    tl_log("Requesting screen recording permissions");
    CGDisplayStreamRef stream = CGDisplayStreamCreate(CGMainDisplayID(), 1, 1, kCVPixelFormatType_32BGRA, nil, ^(CGDisplayStreamFrameStatus status, uint64_t displayTime, IOSurfaceRef frameSurface, CGDisplayStreamUpdateRef updateRef) {
    });
    if (stream)
    {
        pass = true;
        CFRelease(stream);
    }
    if (@available(macOS 10.15, *))
    {
        //CGPreflightScreenCaptureAccess();
        tl_log("MACOS >= 10.15 detected");
        //system("screencapture ~/test.png");
        //fast_capture_desktop("tltest.png");
        /*
        CFArrayRef windowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
        NSUInteger numberOfWindows = CFArrayGetCount(windowList);
        NSUInteger numberOfWindowsWithName = 0;
        for (int idx = 0; idx < numberOfWindows; idx++) {
            NSDictionary *windowInfo = (NSDictionary *)CFArrayGetValueAtIndex(windowList, idx);
            NSString *windowName    = windowInfo[(id)kCGWindowName];
            NSNumber *sharing_state  = windowInfo[(id)kCGWindowSharingState];
            //CGPDFDictionaryGetInteger(windowInfo, kCGWindowSharingState, null);
            char state[128];
            sprintf(state, "Sharing state: %d\n", sharing_state.intValue);
            tl_log(state);
            if (windowName)
            {
                tl_log((char*)[windowName UTF8String]);
                numberOfWindowsWithName++;
            } else {
                //no kCGWindowName detected -> not enabled
                break; //breaking early, numberOfWindowsWithName not increased
            }

        }
        CFRelease(windowList);
        bool allow_sr = numberOfWindows == numberOfWindowsWithName;
        tl_log( allow_sr ? "Screen recording allowed" : "Screen recording not allowed");
        */
        
        /*
        CGDisplayStreamRef stream = CGDisplayStreamCreate(CGMainDisplayID(), 1, 1, kCVPixelFormatType_32BGRA, nil, ^(CGDisplayStreamFrameStatus status, uint64_t displayTime, IOSurfaceRef frameSurface, CGDisplayStreamUpdateRef updateRef) {
            ;
        });
        allow_sr = stream != NULL;
        tl_log( allow_sr ? "Screen recording allowed" : "Screen recording not allowed");
        if (stream)
          CFRelease(stream);
        */
        /*
        tl_log("Requesting screen recording permissions");
        CGDisplayStreamRef stream = CGDisplayStreamCreate(CGMainDisplayID(), 1, 1, kCVPixelFormatType_32BGRA, nil, ^(CGDisplayStreamFrameStatus status, uint64_t displayTime, IOSurfaceRef frameSurface, CGDisplayStreamUpdateRef updateRef) {
        });
        if (stream)
        {
            pass = true;
            CFRelease(stream);
        }
        */
    }
    
    return pass;
}
bool check_accesibility_access()
{
    //NSDictionary *options = @{(__bridge id)kAXTrustedCheckOptionPrompt: @YES};
    /*
    if( accessibilityEnabled )
    {
        tl_log("MACOS >= 10.9");
        
        return accessibilityEnabled;
    }
    tl_log("MACOS < 10.9");
    */
    //return true;
    bool pass = true;
    //pass = AXIsProcessTrusted();
    const void * keys[] = { kAXTrustedCheckOptionPrompt };
    const void * values[] = { kCFBooleanTrue };
    CFDictionaryRef options = CFDictionaryCreate(
        kCFAllocatorDefault,
        keys,
        values,
        sizeof(keys) / sizeof(*keys),
        &kCFCopyStringDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    pass = AXIsProcessTrustedWithOptions((CFDictionaryRef)options);
    return pass;
}
void open_accesibility()
{
    //x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility
    //NSString* prefPage = @"x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility";
    //[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:prefPage]];
    
    //##catalina reset permission
    //tccutil reset All
    //tccutil reset All com.timerlogs.client
}
