//
//  km.h
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//

#ifndef km_h
#define km_h

#import "HookKeyboardMouseData.h"
#import "HookApplicationData.h"

void _clean_km_capture(void);
void start_km_capture(void);
void stop_km_capture(void);
HookKeyboardMouseData* getData(void);
HookApplicationData* getActiveWindowData(void);
#endif /* km_h */
