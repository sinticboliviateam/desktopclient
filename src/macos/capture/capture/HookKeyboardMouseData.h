//
//  HookKeyboardMouseData.h
//  capture
//
//  Created by J. Marcelo Aviles Paco on 2/10/21.
//  Copyright © 2021 TimerLogs. All rights reserved.
//

#ifndef HookKeyboardMouseData_h
#define HookKeyboardMouseData_h

#include "HookApplicationData.h"

typedef struct hookKeyboardMouseData
{
    unsigned long         clicks;
    unsigned long        leftClicks;
    unsigned long        rightClicks;
    unsigned long        keydowns;
    double                keyboardInactivity;
    double                mouseInactivity;
    double					inactivity;
    HookApplicationData    **applications;
}HookKeyboardMouseData;

HookKeyboardMouseData* HookKeyboardMouseData_new(void);
void HookKeyboardMouseData_free(HookKeyboardMouseData* obj);

#endif /* HookKeyboardMouseData_h */
