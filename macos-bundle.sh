#!/bin/bash

rm -fr release
rm -fr buildmacos
meson --prefix=`pwd`/release/TimerLogs.app --bindir=Contents/MacOS buildmacos -Drelease=true
cd buildmacos
meson configure -Drelease=true
meson install

echo "Install [DONE]"

CONTENTS_DIR="../release/TimerLogs.app/Contents"
GTK3_DIR="${CONTENTS_DIR}/Frameworks/gtk3"
SYSTEM_GTK3_DIR=
SYSTEM_GDK_PIXBUF_DIR="/usr/local/Cellar/gdk-pixbuf/2.42.2"
SYSTEM_GDK_PIXBUF_LOADERS_DIR="/usr/local/Cellar/gdk-pixbuf/2.42.2/lib/gdk-pixbuf-2.0/2.10.0/loaders"

function change_lib_path
{
	# echo -e "Looking dependencies for: $1\n"
	local filelib=$1
	local DEP_LIBS=$(otool -L $filelib | grep "/*.*dylib" -o | xargs)
	
	for dependency in $DEP_LIBS; do
		#if [ ! -f $dependency ]; then
			#continue
		#fi
		filename=$(basename $dependency)
		if [[ $dependency == *"/usr/lib/"* || $dependency == *"/System/Library/"* ]]; then
			continue
		fi
		
		new_path="@executable_path/../Frameworks/gtk3/lib/$filename"
		# echo $filename
		if [[ $filename == "libcapture.dylib" ]]; then
			new_path="@executable_path/$filename"
			echo "Setting new path for: ${dependency} -> ${new_path}"
		fi
		
		install_name_tool -change $dependency $new_path $filelib
	done
	echo -e " \n"
}

GTK3_LIBS=$(otool -L ${CONTENTS_DIR}/MacOS/TimerLogs)

#echo $GTK3_LIBS
#for dylib in $GTK3_LIBS; do
#	if [ ! -f $dylib ]; then
#		continue
#	fi
#	filename=$(basename $dylib)
#	if [ $filename = "libSystem.B.dylib" ]; then
#		continue
#	fi
#	echo "Setting new path for: ${filename}"
#	install_name_tool -change $dylib @executable_path/../Frameworks/gtk3/lib/$filename ${CONTENTS_DIR}/MacOS/timerlogs
#done

function set_paths
{
	change_lib_path "${CONTENTS_DIR}/MacOS/TimerLogs"

	for dylib in $(ls $GTK3_DIR/lib); do
		change_lib_path $GTK3_DIR/lib/$dylib
	done

	for dylib in $(ls $GTK3_DIR/lib/gdk-pixbuf-2.0/2.10.0/loaders); do
		change_lib_path $GTK3_DIR/lib/gdk-pixbuf-2.0/2.10.0/loaders/$dylib
	done

	for dylib in $(ls $GTK3_DIR/lib/gtk-3.0/3.0.0/immodules); do
		change_lib_path $GTK3_DIR/lib/gtk-3.0/3.0.0/immodules/$dylib
	done

	for dylib in $(ls $GTK3_DIR/lib/gtk-3.0/3.0.0/printbackends); do
		change_lib_path $GTK3_DIR/lib/gtk-3.0/3.0.0/printbackends/$dylib
	done

	for dylib in $(ls $GTK3_DIR/lib/gio/modules); do
		change_lib_path $GTK3_DIR/lib/gio/modules/$dylib
	done
	
	for dylib in $(ls $GTK3_DIR/lib/guile/3.0/extensions); do
		change_lib_path $GTK3_DIR/lib/guile/3.0/extensions/$dylib
	done
	
}

set_paths

#update loaders cache
pixbuf_loaders_path="@executable_path/../Frameworks/gtk3/lib/gdk-pixbuf-2.0/2.10.0/loaders"
plsp=${pixbuf_loaders_path////\\\/}
pdsp=${SYSTEM_GDK_PIXBUF_LOADERS_DIR////\\\/}

gdk-pixbuf-query-loaders | sed "s/${pdsp}/${plsp}/g" > $GTK3_DIR/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache


