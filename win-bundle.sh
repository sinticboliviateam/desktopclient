#!/bin/bash

rm -fr releasewin
rm -fr buildwin
meson --prefix=`pwd`/releasewin --bindir=bin buildwin -Drelease=true -Dso_target=WIN
cd buildwin
#meson configure -Drelease=true
meson install
cp -v /mingw64/bin/libgee-0.8-2.dll ../releasewin/bin
cp -v /mingw64/bin/libssl-1_1-x64.dll ../releasewin/bin
cp -v /mingw64/bin/libcrypto-1_1-x64.dll ../releasewin/bin
# echo `pwd`
