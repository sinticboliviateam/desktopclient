## 1 Setup your development environment

	1. Mingw64
	2. Valac 0.50.1 or grater
	3. libgee
	4. Make for Mingw64
	5. Gtk 3.24
	
	
## 2 Compiling for windows

In order to get rid the command prompt, be sure to add -mwindows to compiler

-X -mwindows

## DEBUG
mingw32-make.exe -f Makefile.win

### RELEASE
mingw32-make.exe -f Makefile.win release


## MAC OS Environment

requires vala 0.50 or grater

brew install vala
brew install gtk+-3.0
brew install libgee
brew install icu4c
brew install libsoup
brew install meson

# Fix issues for icu package
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/Cellar/icu4c/67.1/lib/pkgconfig
pkg-config --cflags libsoup-2.4

meson build && cd build && meson compile

# If you want to build a release
meson build
cd build
meson configure -Drelease=true
meson compile

